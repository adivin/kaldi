// online2bin/online2-wav-nnet3-latgen-faster.cc

// Copyright 2014    Johns Hopkins University (author: Daniel Povey)
//                     2016    Api.ai (Author: Ilya Platonov)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include "feat/wave-reader.h"
#include "online2/online-nnet3-decoding.h"
#include "online2/online-nnet2-feature-pipeline.h"
#include "online2/onlinebin-util.h"
#include "online2/online-timing.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "lat/word-align-lattice-lexicon.h"
#include "util/kaldi-thread.h"
#include "nnet3/nnet-utils.h"
#include <iostream>


int main(int argc, char *argv[]) {
    try {
        using namespace kaldi;
        using namespace fst;

        typedef kaldi::int32 int32;
        typedef kaldi::int64 int64;

        const char *usage =
                "Reads in wav file(s) and simulates online decoding with neural nets\n"
                "(nnet3 setup), with optional iVector-based speaker adaptation and\n"
                "optional endpointing.    Note: some configuration values and inputs are\n"
                "set via config files whose filenames are passed as options\n"
                "\n"
                "Usage: online2-wav-nnet3-latgen-faster [options] <nnet3-in> <fst-in> "
                "<wav-rspecifier> <align-lex-in> <wsym> <ctm-wxfilename>\n"
                "The spk2utt-rspecifier can just be <utterance-id> <utterance-id> if\n"
                "you want to decode utterance by utterance.\n";

        ParseOptions po(usage);

        std::string word_syms_rxfilename;

        // feature_opts includes configuration for the iVector adaptation,
        // as well as the basic features.
        OnlineNnet2FeaturePipelineConfig feature_opts("/home/seni/AndroidStudioProjects/Ark/app/src/main/assets/model/fbank.conf", "fbank");
        nnet3::NnetSimpleLoopedComputationOptions decodable_opts(1.0, 20);
        LatticeFasterDecoderConfig decoder_opts(5.0, 3000, 6.0, 25);

        BaseFloat chunk_length_secs = 0.18;
        bool online = true;

        po.Register("chunk-length", &chunk_length_secs,
                                "Length of chunk size in seconds, that we process.    Set to <= 0 "
                                "to use all input in one chunk.");
        po.Register("word-symbol-table", &word_syms_rxfilename,
                                "Symbol table for words [for debug output]");
        po.Register("online", &online,
                                "You can set this to false to disable online iVector estimation "
                                "and have all the data for each utterance used, even at "
                                "utterance start.    This is useful where you just want the best "
                                "results and don't care about online operation.    Setting this to "
                                "false has the same effect as setting "
                                "--use-most-recent-ivector=true and --greedy-ivector-extractor=true "
                                "in the file given to --ivector-extraction-config, and "
                                "--chunk-length=-1.");
        po.Register("num-threads-startup", &g_num_threads,
                                "Number of threads used when initializing iVector extractor.");

        //feature_opts.Register(&po);
        //decodable_opts.Register(&po);
        //decoder_opts.Register(&po);


        po.Read(argc, argv);

        if (po.NumArgs() != 6) {
            po.PrintUsage();
            return 1;
        }

        std::string nnet3_rxfilename = po.GetArg(1),
                fst_rxfilename = po.GetArg(2),
                wav_rspecifier = po.GetArg(3),
                align_lex = po.GetArg(4),
                wsyms = po.GetArg(5),
                ctm_wxfilename = po.GetArg(6);

        OnlineNnet2FeaturePipelineInfo feature_info(feature_opts);

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        {
            bool binary;
            Input ki(nnet3_rxfilename, &binary);
            trans_model.Read(ki.Stream(), binary);
            am_nnet.Read(ki.Stream(), binary);
            SetBatchnormTestMode(true, &(am_nnet.GetNnet()));
            SetDropoutTestMode(true, &(am_nnet.GetNnet()));
            nnet3::CollapseModel(nnet3::CollapseModelConfig(), &(am_nnet.GetNnet()));
        }

        // this object contains precomputed stuff that is used by all decodable
        // objects.    It takes a pointer to am_nnet because if it has iVectors it has
        // to modify the nnet to accept iVectors at intervals.
        nnet3::DecodableNnetSimpleLoopedInfo decodable_info(decodable_opts, &am_nnet);


        fst::Fst<fst::StdArc> *decode_fst = ReadFstKaldiGeneric(fst_rxfilename);

        fst::SymbolTable *word_syms = NULL;
        if (word_syms_rxfilename != "")
            if (!(word_syms = fst::SymbolTable::ReadText(word_syms_rxfilename)))
                KALDI_ERR << "Could not read symbol table from file " << word_syms_rxfilename;

        OnlineTimingStats timing_stats;

        std::ifstream is(align_lex, std::ifstream::in);
        std::vector<std::vector<int32>> lexicon;
        ReadLexiconForWordAlign(is, &lexicon);
        WordAlignLatticeLexiconInfo lexicon_info(lexicon);
        WordAlignLatticeLexiconOpts opts;

        Output ko(ctm_wxfilename, false);
        ko.Stream() << std::fixed;
        ko.Stream().precision(2);
        BaseFloat frame_shift = 0.03;
        word_syms = fst::SymbolTable::ReadText(wsyms);

        WaveHolder wavholder;
        std::ifstream wavis(wav_rspecifier, std::ios::binary);
        wavholder.Read(wavis);
        const WaveData &wave_data = wavholder.Value();
        // get the data for channel zero (if the signal is not mono, we only
        // take the first channel).
        SubVector<BaseFloat> data(wave_data.Data(), 0);

        OnlineNnet2FeaturePipeline feature_pipeline(feature_info);

        SingleUtteranceNnet3Decoder decoder(decoder_opts, trans_model,
                                            decodable_info, *decode_fst, &feature_pipeline);
        OnlineTimer decoding_timer("wavfile");

        BaseFloat samp_freq = 16000.0;
        int32 chunk_length;
        if (chunk_length_secs > 0) {
            chunk_length = int32(samp_freq * chunk_length_secs);
            if (chunk_length == 0) chunk_length = 1;
        } else {
            chunk_length = std::numeric_limits<int32>::max();
        }

        int32 samp_offset = 0;
        std::vector<std::pair<int32, BaseFloat> > delta_weights;

        while (samp_offset < data.Dim()) {
            int32 samp_remaining = data.Dim() - samp_offset;
            int32 num_samp = chunk_length < samp_remaining ? chunk_length : samp_remaining;

            SubVector<BaseFloat> wave_part(data, samp_offset, num_samp);
            feature_pipeline.AcceptWaveform(samp_freq, wave_part);

            samp_offset += num_samp;
            decoding_timer.WaitUntil(samp_offset / samp_freq);
            if (samp_offset == data.Dim()) {
                // no more input. flush out last frames
                feature_pipeline.InputFinished();
            }

            decoder.AdvanceDecoding();
        }
        decoder.FinalizeDecoding();

        CompactLattice clat;
        bool end_of_utterance = true;
        decoder.GetLattice(end_of_utterance, &clat);

        decoding_timer.OutputStats(&timing_stats);

        CompactLattice aligned_clat;
        WordAlignLatticeLexicon(clat, trans_model, lexicon_info, opts, &aligned_clat);

        CompactLattice best_path;
        CompactLatticeShortestPath(aligned_clat, &best_path);

        std::vector<int32> words, times, lengths;
        CompactLatticeToWordAlignment(best_path, &words, &times, &lengths);

        for(size_t j=0; j < words.size(); j++) {
            if(words[j] == 0)
                continue;
            ko.Stream() << word_syms->Find(words[j]) << ' ' << (frame_shift * times[j]) << ' '
                        << (frame_shift * lengths[j]) << std::endl;
        }
        KALDI_LOG << "Decoded file";
        timing_stats.Print(online);

        delete decode_fst;
        delete word_syms; // will delete if non-NULL.
        return 0;
    } catch(const std::exception& e) {
        std::cerr << e.what();
        return -1;
    }
} // main()
