#include "base/timer.h"
#include "feat/wave-reader.h"
#include "fstext/fstext-lib.h"
#include "lat/compose-lattice-pruned.h"
#include "lat/lattice-functions.h"
#include "lat/word-align-lattice.h"
#include "nnet3/nnet-utils.h"
#include "online2/online-nnet2-feature-pipeline.h"
#include "online2/online-nnet3-decoding.h"
#include "online2/online-timing.h"
#include "online2/onlinebin-util.h"
#include "rnnlm/rnnlm-lattice-rescoring.h"
#include "rnnlm/rnnlm-utils.h"
#include "lm/const-arpa-lm.h"
#include "util/kaldi-thread.h"
#include <iostream>
#include <string>
#include "stdio.h"

int parseInt(char*& s) {
    int32 n = *s - '0';
    int32 d;
    while((d = *++s - '0') >= 0) {
        n = 10*n + d;
    }
    return n;
}

void readIds(std::string f, std::vector<int32>& v) {
    FILE* num_f = fopen(f.c_str(), "rb");
    if (!num_f) KALDI_ERR << "Could not open file " << f;
    int32 sz;
    fseek(num_f, 0L, SEEK_END);
    sz = ftell(num_f);
    rewind(num_f);
    char text[sz];
    fread(text, 1, sz, num_f);
    char* s = text;
    while(true) {
        int32 offset = (int32) (s - &text[0]);
        if (offset >= sz) break;
        v.push_back(parseInt(s));
        s++;
    }
}

int transcribe_file(std::string wavpath, std::string modeldir, std::string ctm) {
    try {

        using namespace kaldi;
        using namespace kaldi::rnnlm;
        using namespace fst;

        typedef kaldi::int32 int32;
        typedef kaldi::int64 int64;

        Timer timer;
        std::string cmn_path = modeldir + "cmvn.conf";
        {
            ifstream f(cmn_path.c_str());
            if (!f.good()) cmn_path = "";
        }
        // feature_opts includes configuration for the iVector adaptation,
        // as well as the basic features.
        std::string silphones = "1:2:3:4:5:6:7:8:9:10";
        OnlineSilenceWeightingConfig sil_config(0.0001f, "");
        OnlineNnet2FeaturePipelineConfig feature_opts(modeldir + "mfcc.conf", "mfcc", "", sil_config, "", cmn_path);
        nnet3::NnetSimpleLoopedComputationOptions decodable_opts(1.0, 40, 3);
        LatticeFasterDecoderConfig decoder_opts(10.0, 5000, 10.0, 25, 4.5);

        BaseFloat chunk_length_secs = 0.72;


        std::string nnet3_rxfilename = modeldir + "final.mdl",
        fst_rxfilename = modeldir + "HCLG.fst",
        wav_rspecifier = wavpath,
        word_boundary_f = modeldir + "word_boundary.int",
        wsyms = modeldir + "words.txt",
        ctm_wxfilename = ctm;

        fst::SymbolTable* word_syms = fst::SymbolTable::ReadText(wsyms);

        // for LM rescore
        std::vector<int32> ids;
        std::string ids_file = modeldir + "ids.int";
        readIds(ids_file, ids);
        int32 num_words = (int32) word_syms->NumSymbols();
        rnnlm::RnnlmComputeStateComputationOptions rnn_opts(ids[0],
            ids[1], ids[3], ids[2], ids[4], num_words, 145000, modeldir);
        ComposeLatticePrunedOptions compose_opts(4.0, 50000, 1.5, 100);
        BaseFloat rnn_scale = 0.9;
        int32 max_ngram_order = 4;

        std::string lm_to_subtract_fname = modeldir + "o3_1M.carpa",
            word_small_emb_fname = modeldir + "word_embedding_small.final.mat",
            word_med_emb_fname = modeldir + "word_embedding_med.final.mat",
            word_large_emb_fname = modeldir + "word_embedding_large.final.mat",
            rnnlm_raw_fname = modeldir + "final.raw";

        CuMatrix<BaseFloat> word_emb_mat_large;
        ReadKaldiObject(word_large_emb_fname, &word_emb_mat_large);
        CuMatrix<BaseFloat> word_emb_mat_med;
        ReadKaldiObject(word_med_emb_fname, &word_emb_mat_med);
        CuMatrix<BaseFloat> word_emb_mat_small;
        ReadKaldiObject(word_small_emb_fname, &word_emb_mat_small);

        ConstArpaLm* const_arpa = new ConstArpaLm();
        ReadKaldiObject(lm_to_subtract_fname, const_arpa);
        fst::DeterministicOnDemandFst<StdArc> *carpa_lm_to_subtract_fst = NULL;
        carpa_lm_to_subtract_fst = new ConstArpaLmDeterministicFst(*const_arpa);
        fst::ScaleDeterministicOnDemandFst* lm_to_subtract_det_scale = NULL;
        lm_to_subtract_det_scale = new fst::ScaleDeterministicOnDemandFst(-rnn_scale,
            carpa_lm_to_subtract_fst);

        nnet3::Nnet rnnlm;
        ReadKaldiObject(rnnlm_raw_fname, &rnnlm);
        SetDropoutTestMode(true, &(rnnlm));
        SetQuantTestMode(true, &(rnnlm));
        SetBatchnormTestMode(true, &(rnnlm));

        const RnnlmComputeStateInfoAdapt rnn_info(rnn_opts, rnnlm, word_emb_mat_large, word_emb_mat_med, word_emb_mat_small,
                word_emb_mat_large.NumRows(), word_emb_mat_med.NumRows());

        rnnlm::KaldiRnnlmDeterministicFstAdapt* lm_to_add_orig =
            new rnnlm::KaldiRnnlmDeterministicFstAdapt(max_ngram_order, rnn_info);

        fst::DeterministicOnDemandFst<StdArc>* lm_to_add =
            new fst::ScaleDeterministicOnDemandFst(rnn_scale, lm_to_add_orig);

        fst::ComposeDeterministicOnDemandFst<StdArc> combined_lms(lm_to_subtract_det_scale,
                                                                  lm_to_add);
        // end rescore setup
        OnlineNnet2FeaturePipelineInfo feature_info(feature_opts);
        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        {
            bool binary;
            Input ki(nnet3_rxfilename, &binary);

            trans_model.Read(ki.Stream(), binary);
            am_nnet.Read(ki.Stream(), binary);
            SetBatchnormTestMode(true, &(am_nnet.GetNnet()));
            SetDropoutTestMode(true, &(am_nnet.GetNnet()));
            SetQuantTestMode(true, &(am_nnet.GetNnet()));
            // nnet3::CollapseModel(nnet3::CollapseModelConfig(), &(am_nnet.GetNnet()));
        }

        // this object contains precomputed stuff that is used by all decodable
        // objects.    It takes a pointer to am_nnet because if it has iVectors it has
        // to modify the nnet to accept iVectors at intervals.
        nnet3::DecodableNnetSimpleLoopedInfo decodable_info(decodable_opts, &am_nnet);

        fst::Fst<fst::StdArc> *decode_fst = ReadFstKaldiGeneric(fst_rxfilename);

        WordBoundaryInfoNewOpts opts;
        WordBoundaryInfo wordb_info(opts, word_boundary_f);

        Output ko(ctm_wxfilename, false);
        ko.Stream() << std::fixed;
        ko.Stream().precision(2);
        BaseFloat frame_shift = 0.03;

        WaveHolder wavholder;
        std::ifstream wavis(wav_rspecifier, std::ios::binary);
        wavholder.Read(wavis);
        const WaveData &wave_data = wavholder.Value();
        // get the data for channel zero (if the signal is not mono, we only
        // take the first channel).
        SubVector<BaseFloat> data(wave_data.Data(), 0);

        OnlineNnet2FeaturePipeline feature_pipeline(feature_info);

        SingleUtteranceNnet3Decoder decoder(decoder_opts, trans_model, decodable_info, *decode_fst,
                                            &feature_pipeline);

        BaseFloat samp_freq = 16000.0;
        int32 chunk_length;

        if (chunk_length_secs > 0) {
            chunk_length = int32(samp_freq * chunk_length_secs);
        if (chunk_length == 0) chunk_length = 1;
        } else {
            chunk_length = std::numeric_limits<int32>::max();
        }

        OnlineSilenceWeighting silence_weighting(trans_model, feature_info.silence_weighting_config, \
            decodable_opts.frame_subsampling_factor);
        std::vector<std::pair<int32, BaseFloat> > delta_weights;

        int32 samp_offset = 0;
        Timer timerb;
        while (samp_offset < data.Dim()) {
            int32 samp_remaining = data.Dim() - samp_offset;
            int32 num_samp = chunk_length < samp_remaining ? chunk_length : samp_remaining;

            SubVector<BaseFloat> wave_part(data, samp_offset, num_samp);
            feature_pipeline.AcceptWaveform(samp_freq, wave_part);

            samp_offset += num_samp;

            if (samp_offset == data.Dim()) {
                // no more input. flush out last frames
                feature_pipeline.InputFinished();
            }

            if (silence_weighting.Active() && feature_pipeline.IvectorFeature() != NULL) {
                silence_weighting.ComputeCurrentTraceback(decoder.Decoder());
                silence_weighting.GetDeltaWeights(feature_pipeline.NumFramesReady(), 0, &delta_weights);
                feature_pipeline.IvectorFeature()->UpdateFrameWeights(delta_weights);
            }
            decoder.AdvanceDecoding();
        }
        decoder.FinalizeDecoding();

        double section_elapsed_decode = timerb.Elapsed();
        KALDI_LOG << "Decode time " << section_elapsed_decode;
        timerb.Reset();

        kaldi::int64 frame_count = feature_pipeline.NumFramesReady();

        CompactLattice clat;
        bool end_of_utterance = true;
        decoder.GetLattice(end_of_utterance, &clat);

        TopSortCompactLatticeIfNeeded(&clat);

        CompactLattice clat_rescored;
        ComposeCompactLatticePrunedB(compose_opts, clat, &combined_lms, &clat_rescored, max_ngram_order, true);

        CompactLattice best_path;
        CompactLatticeShortestPath(clat, &best_path);

        CompactLattice aligned_clat;
        WordAlignLattice(best_path, trans_model, wordb_info, 0, &aligned_clat);

        std::vector<int32> words, times, lengths;
        CompactLatticeToWordAlignment(aligned_clat, &words, &times, &lengths);

        double section_elapsed_lats = timerb.Elapsed();
        KALDI_LOG << "Lattice ops time " << section_elapsed_lats;

        double elapsed = timer.Elapsed();
        // KALDI_LOG << "Recognition time taken " << section_elapsed_decode + section_elapsed_lats
        //         << "s: real-time factor assuming 100 frames/sec is "
        //         << ((section_elapsed_decode + section_elapsed_lats) * 100.0 / frame_count);
        // KALDI_LOG << "Total time taken " << elapsed
        //         << "s: real-time factor assuming 100 frames/sec is "
        //         << (elapsed * 100.0 / frame_count);

        for(size_t j=0; j < words.size(); j++) {
            if(words[j] == 0)
                continue;
            ko.Stream() << word_syms->Find(words[j]) << ' ' << (frame_shift * times[j]) << ' '
            << (frame_shift * lengths[j]) << std::endl;
        }

        delete decode_fst;
        delete word_syms; // will delete if non-NULL.
    } catch(const std::exception& e) {
        std::cout << "ERROR";
        return 1;
    }
    return 0;
}

int main(int argc, char* argv[])  {
    const char *usage =
        "Does online decoding on wavfile.\n"
        "Usage: nnet3-decode <wav-path> <model-dir> <out-ctm>\n";

    kaldi::ParseOptions po(usage);

    po.Read(argc, argv);

    if (po.NumArgs() != 3) {
        po.PrintUsage();
        return 1;
    }

    std::string wavp = po.GetArg(1);
    std::string modeldir = po.GetArg(2);
    std::string ctm = po.GetArg(3);
    modeldir += "/";
    transcribe_file(wavp, modeldir, ctm);
}
