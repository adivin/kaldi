#include "feat/wave-reader.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "lat/compose-lattice-pruned.h"
#include "util/kaldi-thread.h"
#include "online2/online-nnet3-decoding.h"
#include "online2/online-nnet2-feature-pipeline.h"
#include "online2/onlinebin-util.h"
#include "online2/online-timing.h"
#include "lat/word-align-lattice-lexicon.h"
#include "nnet3/nnet-utils.h"
#include "rnnlm/rnnlm-lattice-rescoring.h"
#include "rnnlm/rnnlm-utils.h"
#include "lm/const-arpa-lm.h"
#include "base/timer.h"
#include <iostream>
#include <string>
#include <thread>

using namespace kaldi;
using namespace kaldi::rnnlm;
using namespace fst;


void readIds(std::string f, std::vector<int32>& v) {
    FILE* num_f = fopen(f.c_str(), "rb");
    if (!num_f) KALDI_ERR << "Could not open file " << f;
    int32 sz;
    fseek(num_f, 0L, SEEK_END);
    sz = ftell(num_f);
    rewind(num_f);
    char text[sz];
    fread(text, 1, sz, num_f);
    char* s = text;
    while(true) {
        int32 offset = (int32) (s - &text[0]);
        if (offset >= sz) break;
        v.push_back(parseInt(s));
        s++;
    }
}

void finish_segment(CompactLattice& clat, TransitionModel &trans_model,
        WordAlignLatticeLexiconInfo &lexicon_info, WordAlignLatticeLexiconOpts& opts,
        ComposeLatticePrunedOptions &compose_opts, fst::ComposeDeterministicOnDemandFst<StdArc> &combined_lms,
        Output& ko, fst::SymbolTable* word_syms, BaseFloat frame_shift,
        int32 max_ngram_order, rnnlm::KaldiRnnlmDeterministicFstAdapt* lm_to_add_orig, fst::DeterministicOnDemandFst<StdArc>* carpa_lm) {

    TopSortCompactLatticeIfNeeded(&clat);

    CompactLattice clat_composed;
    fst::ScaleLattice(fst::GraphLatticeScale(0.), &clat);
    ComposeCompactLatticeDeterministic(clat, carpa_lm, &clat_composed);

    Lattice lat_composed;
    ConvertLattice(clat_composed, &lat_composed);
    Invert(&lat_composed);
    CompactLattice determinized_clat;
    DeterminizeLattice(lat_composed, &determinized_clat);

    TopSortCompactLatticeIfNeeded(&determinized_clat);

    CompactLattice clat_rescored;
    ComposeCompactLatticePrunedB(compose_opts, determinized_clat,
        &combined_lms, &clat_rescored, max_ngram_order, true);

    CompactLattice best_path;
    CompactLatticeShortestPath(clat_rescored, &best_path);

    CompactLattice aligned_clat;
    WordAlignLatticeLexicon(best_path, trans_model, lexicon_info, opts, &aligned_clat);

    std::vector<int32> words, times, lengths;
    CompactLatticeToWordAlignment(aligned_clat, &words, &times, &lengths);

    std::vector<int32> wordseq;
    for(size_t j=0; j < words.size(); j++) {
        int32 w = words[j];
        if(w == 0) continue;
        wordseq.push_back(w);
        ko.Stream() << word_syms->Find(w) << ' ' << (frame_shift * times[j]) << ' '
        << (frame_shift * lengths[j]) << std::endl;
    }
    //lm_to_add_orig->ClearToContinue(wordseq);
    lm_to_add_orig->Clear();  // TODO: check why ClearToContinue is worse

}

int transcribe_file(std::string wavpath, std::string modeldir, std::string ctm) {
    try {

        Timer timer;

        // feature_opts includes configuration for the iVector adaptation,
        // as well as the basic features.
        int32 frame_subsampling_factor = 3;

        std::string silence_phones;
        {
            std::ifstream is(modeldir + "silence.csl", std::ifstream::in);
            if (!is.is_open()) KALDI_ERR << "No silence phones file";
            while (std::getline(is, silence_phones)) {
                break;
            }
        }

        OnlineSilenceWeightingConfig sil_config(0.0001f, "");
        OnlineNnet2FeaturePipelineConfig feature_opts(modeldir + "mfcc.conf", "mfcc", "", sil_config, "");
        nnet3::NnetSimpleLoopedComputationOptions decodable_opts(1.0, 40, frame_subsampling_factor);
        LatticeFasterDecoderConfig decoder_opts(10.0, 5000, 6.0, 25, 6.0);

        BaseFloat chunk_length_secs = 0.24;

        std::string nnet3_rxfilename = modeldir + "final.mdl",
        fst_rxfilename = modeldir + "HCLG.fst",
        wav_rspecifier = wavpath,
        align_lex = modeldir + "align_lexicon.bin",
        wsyms = modeldir + "words.txt",
        ctm_wxfilename = ctm;

        fst::SymbolTable* word_syms = fst::SymbolTable::ReadText(wsyms);

        // for LM rescore

        std::string lm_carpa = modeldir + "o3_2p5M.carpa",
            word_feat_fname = modeldir + "word_feats.bin",
            word_small_emb_fname = modeldir + "word_embedding_small.final.mat",
            word_med_emb_fname = modeldir + "word_embedding_med.final.mat",
            word_large_emb_fname = modeldir + "word_embedding_large.final.mat",
            rnnlm_raw_fname = modeldir + "final.raw";

        CuMatrix<BaseFloat> word_emb_mat_large;
        ReadKaldiObject(word_large_emb_fname, &word_emb_mat_large);
        CuMatrix<BaseFloat> word_emb_mat_med;
        ReadKaldiObject(word_med_emb_fname, &word_emb_mat_med);
        CuMatrix<BaseFloat> word_emb_mat_small;
        ReadKaldiObject(word_small_emb_fname, &word_emb_mat_small);

        std::vector<int32> ids;
        std::string ids_file = modeldir + "ids.int";
        readIds(ids_file, ids);
        int32 num_words = (int32) word_syms->NumSymbols();
        rnnlm::RnnlmComputeStateComputationOptions rnn_opts(ids[0],
            ids[1], ids[3], ids[2], ids[4], num_words, word_emb_mat_large.NumRows() +
            word_emb_mat_med.NumRows() + word_emb_mat_small.NumRows(), modeldir);
        ComposeLatticePrunedOptions compose_opts(4.0, 500, 1.25, 75);
        BaseFloat rnn_scale = 0.9;
        int32 max_ngram_order = 4;

        ConstArpaLm* const_arpa = new ConstArpaLm();
        ReadKaldiObject(lm_carpa, const_arpa);
        fst::DeterministicOnDemandFst<StdArc> *carpa_lm_fst = NULL;
        carpa_lm_fst = new ConstArpaLmDeterministicFst(*const_arpa);
        fst::ScaleDeterministicOnDemandFst* carpa_lm_fst_subtract = NULL;
        carpa_lm_fst_subtract = new fst::ScaleDeterministicOnDemandFst(-rnn_scale,
            carpa_lm_fst);

        nnet3::Nnet rnnlm;
        ReadKaldiObject(rnnlm_raw_fname, &rnnlm);

        const RnnlmComputeStateInfoAdapt rnn_info(rnn_opts, rnnlm, word_emb_mat_large, word_emb_mat_med, word_emb_mat_small, 10000, 50000);

        rnnlm::KaldiRnnlmDeterministicFstAdapt* lm_to_add_orig =
            new rnnlm::KaldiRnnlmDeterministicFstAdapt(max_ngram_order, rnn_info);

        fst::DeterministicOnDemandFst<StdArc>* lm_to_add =
            new fst::ScaleDeterministicOnDemandFst(rnn_scale, lm_to_add_orig);

        fst::ComposeDeterministicOnDemandFst<StdArc> combined_lms(carpa_lm_fst_subtract,
                                                                  lm_to_add);
        // end rescore setup

        OnlineNnet2FeaturePipelineInfo feature_info(feature_opts);

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        {
            bool binary;
            Input ki(nnet3_rxfilename, &binary);

            trans_model.Read(ki.Stream(), binary);
            am_nnet.Read(ki.Stream(), binary);
            SetBatchnormTestMode(true, &(am_nnet.GetNnet()));
            SetDropoutTestMode(true, &(am_nnet.GetNnet()));
            nnet3::CollapseModel(nnet3::CollapseModelConfig(), &(am_nnet.GetNnet()));
        }

        // this object contains precomputed stuff that is used by all decodable
        // objects.    It takes a pointer to am_nnet because if it has iVectors it has
        // to modify the nnet to accept iVectors at intervals.
        nnet3::DecodableNnetSimpleLoopedInfo decodable_info(decodable_opts, &am_nnet);

        fst::Fst<fst::StdArc> *decode_fst = ReadFstKaldiGeneric(fst_rxfilename);

        std::vector<std::vector<int32>> lexicon;
        {
            ReadLexiconForWordAlignBin(align_lex, &lexicon);
        }
        WordAlignLatticeLexiconInfo lexicon_info(lexicon);
        WordAlignLatticeLexiconOpts opts;

        Output ko(ctm_wxfilename, false);
        ko.Stream() << std::fixed;
        ko.Stream().precision(2);
        BaseFloat frame_shift = 0.03;

        WaveHolder wavholder;
        std::ifstream wavis(wav_rspecifier, std::ios::binary);
        wavholder.Read(wavis);
        const WaveData &wave_data = wavholder.Value();
        // get the data for channel zero (if the signal is not mono, we only
        // take the first channel).
        SubVector<BaseFloat> data(wave_data.Data(), 0);

        OnlineNnet2FeaturePipeline feature_pipeline(feature_info);

        SingleUtteranceNnet3Decoder decoder(decoder_opts, trans_model, decodable_info, *decode_fst,
                                            &feature_pipeline);

        BaseFloat samp_freq = 16000.0;
        int32 chunk_length;

        if (chunk_length_secs > 0) {
            chunk_length = int32(samp_freq * chunk_length_secs);
        if (chunk_length == 0) chunk_length = 1;
        } else {
            chunk_length = std::numeric_limits<int32>::max();
        }


        OnlineSilenceWeighting silence_weighting(trans_model, feature_info.silence_weighting_config, \
            decodable_opts.frame_subsampling_factor);
        std::vector<std::pair<int32, BaseFloat> > delta_weights;

        int32 samp_offset = 0;
        CompactLattice clat_torescore;
        CompactLattice clat_rtrescored;
        CompactLattice clat_bestpath;
        CompactLattice clat;
        //CompactLatticeWriter clat_writer("ark:clat_best.lat");

        Timer timerb;
        //Timer timerc;
        std::thread t;
        int32 frames_decoded = 0;
        while (samp_offset < data.Dim()) {

            int32 samp_remaining = data.Dim() - samp_offset;
            int32 num_samp = chunk_length < samp_remaining ? chunk_length : samp_remaining;

            SubVector<BaseFloat> wave_part(data, samp_offset, num_samp);
            feature_pipeline.AcceptWaveform(samp_freq, wave_part);

            samp_offset += num_samp;

            if (samp_offset == data.Dim()) {
                // no more input. flush out last frames
                feature_pipeline.InputFinished();
            }

            if (silence_weighting.Active() && feature_pipeline.IvectorFeature() != NULL) {
                silence_weighting.ComputeCurrentTraceback(decoder.Decoder());
                silence_weighting.GetDeltaWeights(feature_pipeline.NumFramesReady(), 0, &delta_weights);
                feature_pipeline.IvectorFeature()->UpdateFrameWeights(delta_weights);
            }

            decoder.AdvanceDecoding();
            if (decoder.isPruneTime() == true) {
                decoder.GetLattice(false, &clat_torescore);
                TopSortCompactLatticeIfNeeded(&clat_torescore);

                CompactLattice clat_composed;
                fst::ScaleLattice(fst::GraphLatticeScale(0.), &clat);
                ComposeCompactLatticeDeterministic(clat_torescore, carpa_lm_fst, &clat_composed);

                Lattice lat_composed;
                ConvertLattice(clat_composed, &lat_composed);
                Invert(&lat_composed);
                CompactLattice determinized_clat;
                DeterminizeLattice(lat_composed, &determinized_clat);

                TopSortCompactLatticeIfNeeded(&determinized_clat);

                if (t.joinable()) t.join();

                ComposeCompactLatticePrunedB(compose_opts, determinized_clat,
                   &combined_lms, &clat_rtrescored, max_ngram_order, false);
                //double tt = timerc.Elapsed();
                //KALDI_LOG << "resc time " <<tt;
                CompactLatticeShortestPath(clat_rtrescored, &clat_bestpath);
                decoder.AdjustCostsWithClatCorrect(&clat_bestpath);
                decoder.StrictPrune();
            }

            if (decoder.isStopTime(silence_phones, trans_model, frame_subsampling_factor)) {
                if (t.joinable()) t.join();
                bool end_of_utterance = false;
                decoder.GetLattice(end_of_utterance, &clat);

                t = std::thread(finish_segment, std::ref(clat), std::ref(trans_model),
                        std::ref(lexicon_info), std::ref(opts), std::ref(compose_opts),
                        std::ref(combined_lms), std::ref(ko), word_syms,
                        frame_shift, max_ngram_order, lm_to_add_orig, carpa_lm_fst);
                frames_decoded += decoder.NumFramesDecoded();
                decoder.InitDecoding(frames_decoded);
            }

        }

        double section_elapsed_decode = timerb.Elapsed();
        KALDI_LOG << "Decode time " << section_elapsed_decode;
        timerb.Reset();

        decoder.FinalizeDecoding();

        kaldi::int64 frame_count = feature_pipeline.NumFramesReady();

        if (decoder.NumFramesDecoded() != 0) {
            bool end_of_utterance = true;
            CompactLattice clat_fin;
            decoder.GetLattice(end_of_utterance, &clat_fin);
            TopSortCompactLatticeIfNeeded(&clat_fin);
            if (t.joinable()) t.join();
            ComposeCompactLatticePrunedB(compose_opts, clat_fin,
                &combined_lms, &clat_rtrescored, max_ngram_order, true);

            CompactLatticeShortestPath(clat_rtrescored, &clat_bestpath);

            CompactLattice aligned_clat;
            WordAlignLatticeLexicon(clat_bestpath, trans_model, lexicon_info, opts, &aligned_clat);

            std::vector<int32> words, times, lengths;
            CompactLatticeToWordAlignment(aligned_clat, &words, &times, &lengths);

            for(size_t j=0; j < words.size(); j++) {
                if(words[j] == 0)
                    continue;
                ko.Stream() << word_syms->Find(words[j]) << ' ' << (frame_shift * times[j]) << ' '
                << (frame_shift * lengths[j]) << std::endl;
            }
        }
        if (t.joinable()) t.join();

        double section_elapsed_lats = timerb.Elapsed();
        KALDI_LOG << "Lattice ops time " << section_elapsed_lats;

        double elapsed = timer.Elapsed();
        KALDI_LOG << "Recognition time taken "<< section_elapsed_decode + section_elapsed_lats
              << "s: real-time factor assuming 100 frames/sec is "
              << ((section_elapsed_decode + section_elapsed_lats) * 100.0 / frame_count);
        KALDI_LOG << "Total time taken "<< elapsed
              << "s: real-time factor assuming 100 frames/sec is "
              << (elapsed * 100.0 / frame_count);

        delete decode_fst;
        delete word_syms; // will delete if non-NULL.
        // delete carpa_lm_to_subtract_fst;
        delete carpa_lm_fst;
        delete carpa_lm_fst_subtract;
    } catch(const std::exception& e) {
        std::cout << "ERROR";
        return 1;
    }
    return 0;
}


int main(int argc, char* argv[])  {
    const char *usage =
        "Does online decoding on wavfile.\n"
        "Usage: nnet3-decode <wav-path> <model-dir> <out-ctm>\n";

    kaldi::ParseOptions po(usage);

    po.Read(argc, argv);

    if (po.NumArgs() != 3) {
        po.PrintUsage();
        return 1;
    }

    std::string wavp = po.GetArg(1);
    std::string modeldir = po.GetArg(2);
    std::string ctm = po.GetArg(3);
    transcribe_file(wavp, modeldir, ctm);
}
