#include "feat/wave-reader.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "lat/compose-lattice-pruned.h"
#include "util/kaldi-thread.h"
#include "online2/online-nnet3-decoding.h"
#include "online2/online-nnet2-feature-pipeline.h"
#include "online2/onlinebin-util.h"
#include "online2/online-timing.h"
#include "lat/word-align-lattice-lexicon.h"
#include "nnet3/nnet-utils.h"
#include "base/timer.h"
#include "rnnlm/rnnlm-lattice-rescoring.h"
#include "rnnlm/rnnlm-utils.h"
#include <iostream>
#include <string>

using namespace kaldi;
using namespace kaldi::rnnlm;
using namespace fst;

void finish_segment(SingleUtteranceNnet3Decoder &decoder, TransitionModel &trans_model,
        OnlineNnet2FeaturePipeline &feature_pipeline, WordAlignLatticeLexiconInfo &lexicon_info,
        WordAlignLatticeLexiconOpts& opts, Output& ko,
        fst::SymbolTable* word_syms, BaseFloat frame_shift) {

    CompactLattice clat;
    bool end_of_utterance = true;

    decoder.GetLattice(end_of_utterance, &clat);
    TopSortCompactLatticeIfNeeded(&clat);

    CompactLattice best_path;
    CompactLatticeShortestPath(clat, &best_path);

    CompactLattice aligned_clat;
    WordAlignLatticeLexicon(best_path, trans_model, lexicon_info, opts, &aligned_clat);

    std::vector<int32> words, times, lengths;
    CompactLatticeToWordAlignment(aligned_clat, &words, &times, &lengths);

    std::vector<int32> words_noeps;
    for(size_t j=0; j < words.size(); j++) {
        int32 w = words[j];
        if(w == 0)
            continue;
        words_noeps.push_back(w);
        ko.Stream() << word_syms->Find(w) << ' ' << (frame_shift * times[j]) << ' '
        << (frame_shift * lengths[j]) << std::endl;
    }

}

int transcribe_file(std::string wavpath, std::string modeldir, std::string ctm) {
    try {

        Timer timer;

        std::string cmn_path = modeldir + "cmvn.conf";
        {
            ifstream f(cmn_path.c_str());
            if (!f.good()) cmn_path = "";
        }
        // feature_opts includes configuration for the iVector adaptation,
        // as well as the basic features.
        int32 frame_subsampling_factor = 3;
        std::string silence_phones = "1:2:3:4:5:6:7:8:9:10";
        OnlineSilenceWeightingConfig sil_config(0.001f, silence_phones);
        OnlineNnet2FeaturePipelineConfig feature_opts(modeldir + "mfcc.conf", "mfcc", "", sil_config, "", cmn_path);
        nnet3::NnetSimpleLoopedComputationOptions decodable_opts(1.0, 40, 3);
        LatticeFasterDecoderConfig decoder_opts(7.0, 3000, 7.0, 25, 6.0);

        BaseFloat chunk_length_secs = 0.72;

        std::string nnet3_rxfilename = modeldir + "final.mdl",
        fst_rxfilename = modeldir + "HCLG.fst",
        wav_rspecifier = wavpath,
        align_lex = modeldir + "align_lexicon.bin",
        wsyms = modeldir + "words.txt",
        ctm_wxfilename = ctm;

        OnlineNnet2FeaturePipelineInfo feature_info(feature_opts);

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        {
            bool binary;
            Input ki(nnet3_rxfilename, &binary);

            trans_model.Read(ki.Stream(), binary);
            am_nnet.Read(ki.Stream(), binary);
            SetBatchnormTestMode(true, &(am_nnet.GetNnet()));
            SetDropoutTestMode(true, &(am_nnet.GetNnet()));
            SetQuantTestMode(true, &(am_nnet.GetNnet()));
            // nnet3::CollapseModel(nnet3::CollapseModelConfig(), &(am_nnet.GetNnet()));
        }

        // this object contains precomputed stuff that is used by all decodable
        // objects.    It takes a pointer to am_nnet because if it has iVectors it has
        // to modify the nnet to accept iVectors at intervals.
        nnet3::DecodableNnetSimpleLoopedInfo decodable_info(decodable_opts, &am_nnet);

        fst::Fst<fst::StdArc> *decode_fst = ReadFstKaldiGeneric(fst_rxfilename);

        std::vector<std::vector<int32>> lexicon;
        {
            ReadLexiconForWordAlignBin(align_lex, &lexicon);
        }
        WordAlignLatticeLexiconInfo lexicon_info(lexicon);
        WordAlignLatticeLexiconOpts opts;

        Output ko(ctm_wxfilename, false);
        ko.Stream() << std::fixed;
        ko.Stream().precision(2);
        BaseFloat frame_shift = 0.03;
        fst::SymbolTable* word_syms = fst::SymbolTable::ReadText(wsyms);

        WaveHolder wavholder;

        std::ifstream wavis(wav_rspecifier, std::ios::binary);
        wavholder.Read(wavis);
        const WaveData &wave_data = wavholder.Value();
        // get the data for channel zero (if the signal is not mono, we only
        // take the first channel).
        SubVector<BaseFloat> data(wave_data.Data(), 0);

        OnlineSilenceWeighting silence_weighting(trans_model, feature_info.silence_weighting_config, \
            decodable_opts.frame_subsampling_factor);

        OnlineNnet2FeaturePipeline feature_pipeline(feature_info);

        SingleUtteranceNnet3Decoder decoder(decoder_opts, trans_model, decodable_info, *decode_fst,
                                            &feature_pipeline);

        BaseFloat samp_freq = 16000.0;
        int32 chunk_length;

        if (chunk_length_secs > 0) {
            chunk_length = int32(samp_freq * chunk_length_secs);
        if (chunk_length == 0) chunk_length = 1;
        } else {
            chunk_length = std::numeric_limits<int32>::max();
        }

        int32 samp_offset = 0;
        int32 frames_decoded = 0;
        Timer timerb;
        while (samp_offset < data.Dim()) {
            int32 samp_remaining = data.Dim() - samp_offset;
            int32 num_samp = chunk_length < samp_remaining ? chunk_length : samp_remaining;

            SubVector<BaseFloat> wave_part(data, samp_offset, num_samp);
            feature_pipeline.AcceptWaveform(samp_freq, wave_part);

            samp_offset += num_samp;

            if (samp_offset == data.Dim()) {
                // no more input. flush out last frames
                feature_pipeline.InputFinished();
            }

            decoder.AdvanceDecoding();

            if (decoder.isStopTime(silence_phones, trans_model, frame_subsampling_factor)) {
                KALDI_LOG << "stopping";
                finish_segment(decoder, trans_model, feature_pipeline, lexicon_info,
                    opts, ko, word_syms, frame_shift);
                frames_decoded += decoder.NumFramesDecoded();
                decoder.InitDecoding(frames_decoded);
            }
        }

        double section_elapsed_decode = timerb.Elapsed();
        KALDI_LOG << "Decode time " << section_elapsed_decode;
        timerb.Reset();

        decoder.FinalizeDecoding();

        kaldi::int64 frame_count = feature_pipeline.NumFramesReady();

        CompactLattice clat;
        bool end_of_utterance = true;

        if (decoder.NumFramesDecoded() != 0) {
            decoder.GetLattice(end_of_utterance, &clat);
            TopSortCompactLatticeIfNeeded(&clat);

            CompactLattice best_path;
            CompactLatticeShortestPath(clat, &best_path);

            CompactLattice aligned_clat;
            WordAlignLatticeLexicon(best_path, trans_model, lexicon_info, opts, &aligned_clat);

            std::vector<int32> words, times, lengths;
            CompactLatticeToWordAlignment(aligned_clat, &words, &times, &lengths);

            for(size_t j=0; j < words.size(); j++) {
                if(words[j] == 0)
                    continue;
                ko.Stream() << word_syms->Find(words[j]) << ' ' << (frame_shift * times[j]) << ' '
                << (frame_shift * lengths[j]) << std::endl;
            }
        }

        double section_elapsed_lats = timerb.Elapsed();
        KALDI_LOG << "Lattice ops time " << section_elapsed_lats;

        double elapsed = timer.Elapsed();
        KALDI_LOG << "Recognition time taken "<< section_elapsed_decode + section_elapsed_lats
              << "s: real-time factor assuming 100 frames/sec is "
              << ((section_elapsed_decode + section_elapsed_lats) * 100.0 / frame_count);
        KALDI_LOG << "Total time taken "<< elapsed
              << "s: real-time factor assuming 100 frames/sec is "
              << (elapsed * 100.0 / frame_count);

        delete decode_fst;
        delete word_syms; // will delete if non-NULL.
    } catch(const std::exception& e) {
        std::cout << "ERROR";
        return 1;
    }
    return 0;
}


int main(int argc, char* argv[])  {
    const char *usage =
        "Does online decoding on wavfile.\n"
        "Usage: nnet3-decode <wav-path> <model-dir> <out-ctm>\n";

    kaldi::ParseOptions po(usage);

    po.Read(argc, argv);

    if (po.NumArgs() != 3) {
        po.PrintUsage();
        return 1;
    }

    std::string wavp = po.GetArg(1);
    std::string modeldir = po.GetArg(2);
    std::string ctm = po.GetArg(3);
    modeldir += "/";
    transcribe_file(wavp, modeldir, ctm);
}
