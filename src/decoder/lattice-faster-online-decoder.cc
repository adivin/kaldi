// decoder/lattice-faster-online-decoder.cc

// Copyright 2009-2012  Microsoft Corporation  Mirko Hannemann
//           2013-2014  Johns Hopkins University (Author: Daniel Povey)
//                2014  Guoguo Chen
//                2014  IMSL, PKU-HKUST (author: Wei Shi)
//                2018  Zhehuai Chen

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

// see note at the top of lattice-faster-decoder.cc, about how to maintain this
// file in sync with lattice-faster-decoder.cc

#include "decoder/lattice-faster-online-decoder.h"
#include "lat/lattice-functions.h"
#include "base/kaldi-math.h"
#include <algorithm>
#include <string>
#include "base/timer.h"
#include <unordered_map>

namespace kaldi {


template <typename FST>
bool LatticeFasterOnlineDecoderTpl<FST>::TestGetBestPath(
    bool use_final_probs) const {
  Lattice lat1;
  {
    Lattice raw_lat;
    this->GetRawLattice(&raw_lat, use_final_probs);
    ShortestPath(raw_lat, &lat1);
  }
  Lattice lat2;
  GetBestPath(&lat2, use_final_probs);
  BaseFloat delta = 0.1;
  int32 num_paths = 1;
  if (!fst::RandEquivalent(lat1, lat2, num_paths, delta, rand())) {
    KALDI_WARN << "Best-path test failed";
    return false;
  } else {
    return true;
  }
}


// Outputs an FST corresponding to the single best path through the lattice.
template <typename FST>
bool LatticeFasterOnlineDecoderTpl<FST>::GetBestPath(Lattice *olat,
                                                     bool use_final_probs) const {
  olat->DeleteStates();
  BaseFloat final_graph_cost;
  BestPathIterator iter = BestPathEnd(use_final_probs, &final_graph_cost);
  if (iter.Done())
    return false;  // would have printed warning.
  StateId state = olat->AddState();
  olat->SetFinal(state, LatticeWeight(final_graph_cost, 0.0));
  while (!iter.Done()) {
    LatticeArc arc;
    iter = TraceBackBestPath(iter, &arc);
    arc.nextstate = state;
    StateId new_state = olat->AddState();
    olat->AddArc(new_state, arc);
    state = new_state;
  }
  olat->SetStart(state);
  return true;
}

template <typename FST>
typename LatticeFasterOnlineDecoderTpl<FST>::BestPathIterator LatticeFasterOnlineDecoderTpl<FST>::BestPathEnd(
    bool use_final_probs,
    BaseFloat *final_cost_out) const {
  if (this->decoding_finalized_ && !use_final_probs)
    KALDI_ERR << "You cannot call FinalizeDecoding() and then call "
              << "BestPathEnd() with use_final_probs == false";
  KALDI_ASSERT(this->NumFramesDecoded() > 0 &&
               "You cannot call BestPathEnd if no frames were decoded.");

  unordered_map<Token*, BaseFloat> final_costs_local;

  const unordered_map<Token*, BaseFloat> &final_costs =
      (this->decoding_finalized_ ? this->final_costs_ :final_costs_local);
  if (!this->decoding_finalized_ && use_final_probs)
    this->ComputeFinalCosts(&final_costs_local, NULL, NULL);

  // Singly linked list of tokens on last frame (access list through "next"
  // pointer).
  BaseFloat best_cost = std::numeric_limits<BaseFloat>::infinity();
  BaseFloat best_final_cost = 0;
  Token *best_tok = NULL;
  for (Token *tok = this->active_toks_.back().toks;
       tok != NULL; tok = tok->next) {
    BaseFloat cost = tok->tot_cost, final_cost = 0.0;
    if (use_final_probs && !final_costs.empty()) {
      // if we are instructed to use final-probs, and any final tokens were
      // active on final frame, include the final-prob in the cost of the token.
      typename unordered_map<Token*, BaseFloat>::const_iterator
          iter = final_costs.find(tok);
      if (iter != final_costs.end()) {
        final_cost = iter->second;
        cost += final_cost;
      } else {
        cost = std::numeric_limits<BaseFloat>::infinity();
      }
    }
    if (cost < best_cost) {
      best_cost = cost;
      best_tok = tok;
      best_final_cost = final_cost;
    }
  }
  if (best_tok == NULL) {  // this should not happen, and is likely a code error or
    // caused by infinities in likelihoods, but I'm not making
    // it a fatal error for now.
    KALDI_WARN << "No final token found.";
  }
  if (final_cost_out)
    *final_cost_out = best_final_cost;
  return BestPathIterator(best_tok, this->NumFramesDecoded() - 1);
}


template <typename FST>
typename LatticeFasterOnlineDecoderTpl<FST>::BestPathIterator LatticeFasterOnlineDecoderTpl<FST>::TraceBackBestPath(
    BestPathIterator iter, LatticeArc *oarc) const {
  KALDI_ASSERT(!iter.Done() && oarc != NULL);
  Token *tok = static_cast<Token*>(iter.tok);
  int32 cur_t = iter.frame, ret_t = cur_t;
  if (tok->backpointer != NULL) {
    ForwardLinkT *link;
    for (link = tok->backpointer->links;
         link != NULL; link = link->next) {
      if (link->next_tok == tok) { // this is the link to "tok"
        oarc->ilabel = link->ilabel;
        oarc->olabel = link->olabel;
        BaseFloat graph_cost = link->graph_cost,
            acoustic_cost = link->acoustic_cost;
        if (link->ilabel != 0) {
          KALDI_ASSERT(static_cast<size_t>(cur_t) < this->cost_offsets_.size());
          acoustic_cost -= this->cost_offsets_[cur_t];
          ret_t--;
        }
        oarc->weight = LatticeWeight(graph_cost, acoustic_cost);
        break;
      }
    }
    if (link == NULL) { // Did not find correct link.
      KALDI_ERR << "Error tracing best-path back (likely "
                << "bug in token-pruning algorithm)";
    }
  } else {
    oarc->ilabel = 0;
    oarc->olabel = 0;
    oarc->weight = LatticeWeight::One(); // zero costs.
  }
  return BestPathIterator(tok->backpointer, ret_t);
}

template <typename FST>
bool LatticeFasterOnlineDecoderTpl<FST>::GetRawLatticePruned(
    Lattice *ofst,
    bool use_final_probs,
    BaseFloat beam) const {
  typedef LatticeArc Arc;
  typedef Arc::StateId StateId;
  typedef Arc::Weight Weight;
  typedef Arc::Label Label;

  // Note: you can't use the old interface (Decode()) if you want to
  // get the lattice with use_final_probs = false.  You'd have to do
  // InitDecoding() and then AdvanceDecoding().
  if (this->decoding_finalized_ && !use_final_probs)
    KALDI_ERR << "You cannot call FinalizeDecoding() and then call "
              << "GetRawLattice() with use_final_probs == false";

  unordered_map<Token*, BaseFloat> final_costs_local;

  const unordered_map<Token*, BaseFloat> &final_costs =
      (this->decoding_finalized_ ? this->final_costs_ : final_costs_local);
  if (!this->decoding_finalized_ && use_final_probs)
    this->ComputeFinalCosts(&final_costs_local, NULL, NULL);

  ofst->DeleteStates();
  // num-frames plus one (since frames are one-based, and we have
  // an extra frame for the start-state).
  int32 num_frames = this->active_toks_.size() - 1;
  KALDI_ASSERT(num_frames > 0);
  for (int32 f = 0; f <= num_frames; f++) {
    if (this->active_toks_[f].toks == NULL) {
      KALDI_WARN << "No tokens active on frame " << f
                 << ": not producing lattice.\n";
      return false;
    }
  }
  unordered_map<Token*, StateId> tok_map;
  std::queue<std::pair<Token*, int32> > tok_queue;
  // First initialize the queue and states.  Put the initial state on the queue;
  // this is the last token in the list active_toks_[0].toks.
  for (Token *tok = this->active_toks_[0].toks;
       tok != NULL; tok = tok->next) {
    if (tok->next == NULL) {
      tok_map[tok] = ofst->AddState();
      ofst->SetStart(tok_map[tok]);
      std::pair<Token*, int32> tok_pair(tok, 0);  // #frame = 0
      tok_queue.push(tok_pair);
    }
  }

  // Next create states for "good" tokens
  while (!tok_queue.empty()) {
    std::pair<Token*, int32> cur_tok_pair = tok_queue.front();
    tok_queue.pop();
    Token *cur_tok = cur_tok_pair.first;
    int32 cur_frame = cur_tok_pair.second;
    KALDI_ASSERT(cur_frame >= 0 &&
                 cur_frame <= this->cost_offsets_.size());

    typename unordered_map<Token*, StateId>::const_iterator iter =
        tok_map.find(cur_tok);
    KALDI_ASSERT(iter != tok_map.end());
    StateId cur_state = iter->second;

    for (ForwardLinkT *l = cur_tok->links;
         l != NULL;
         l = l->next) {
      Token *next_tok = l->next_tok;
      if (next_tok->extra_cost < beam) {
        // so both the current and the next token are good; create the arc
        int32 next_frame = l->ilabel == 0 ? cur_frame : cur_frame + 1;
        StateId nextstate;
        if (tok_map.find(next_tok) == tok_map.end()) {
          nextstate = tok_map[next_tok] = ofst->AddState();
          tok_queue.push(std::pair<Token*, int32>(next_tok, next_frame));
        } else {
          nextstate = tok_map[next_tok];
        }
        BaseFloat cost_offset = (l->ilabel != 0 ?
                                 this->cost_offsets_[cur_frame] : 0);
        Arc arc(l->ilabel, l->olabel,
                Weight(l->graph_cost, l->acoustic_cost - cost_offset),
                nextstate);
        ofst->AddArc(cur_state, arc);
      }
    }
    if (cur_frame == num_frames) {
      if (use_final_probs && !final_costs.empty()) {
        typename unordered_map<Token*, BaseFloat>::const_iterator iter =
            final_costs.find(cur_tok);
        if (iter != final_costs.end())
          ofst->SetFinal(cur_state, LatticeWeight(iter->second, 0));
      } else {
        ofst->SetFinal(cur_state, LatticeWeight::One());
      }
    }
  }
  return (ofst->NumStates() != 0);
}


template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::PruneTokensAfterRescore() {
  int32 start_frame = this->last_pruned_frame - prune_offset_*2;
  if (start_frame < 0) start_frame = 0;
  // -2 because of weird error in processnonemitting otherwise
  for (int32 f = this->active_toks_.size() - 2; f >= start_frame; f--) {
    Token* prev_tok = NULL;
    for (Token* tok = this->active_toks_[f].toks; tok != NULL; ) {
      Token* next_tok = tok->next;
      if (tok->tot_cost > deadtok_cost) {
        this->DeleteForwardLinks(tok);
        delete tok;
        if (prev_tok == NULL) this->active_toks_[f].toks = next_tok;
        else prev_tok->next = next_tok;
        num_toks_--;
      } else {
        prev_tok = tok;
      }
      tok = next_tok;
    }
  }
}

template <typename FST>
bool LatticeFasterOnlineDecoderTpl<FST>::isPruneTime() {
    int32 curr_frame = this->active_toks_.size() - 1;
    if (curr_frame < (this->last_pruned_frame + 2*prune_offset_)) {
        return false;
    }
    return true;
}


bool PrintBnd(int32 c, int32 s, int32 e) {
    if (e == 0) {
        return true;
    } else if (c > s && c < e) {
        return true;
    }
    return false;
}

template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::RecalcTotCosts(int32 start_frame) {
    if (start_frame < 0) start_frame = 0;
    int32 last_frame = this->active_toks_.size() - 1;

    for(int32 i=start_frame+1; i <= last_frame; i++) {
        for(Token* tok = this->active_toks_[i].toks; tok != NULL; tok=tok->next) {
            tok->tot_cost = deadtok_cost + 100.0;
        }
    }

    bool changed;
    for(int32 i=start_frame; i <= last_frame; i++) {
        // have to process frame until no change as due to eps transitions could be
        // changing cost of token that is on the same frame (but was already processed
        // and so the change won't be taken into account if just iterating over tokens once)
        changed = true;
        while (changed == true) {
            changed = false;
            for(Token* tok = this->active_toks_[i].toks; tok != NULL; tok=tok->next) {
                BaseFloat tok_cost = tok->tot_cost;
                for(ForwardLinkT* l = tok->links; l!=NULL; l=l->next) {
                    BaseFloat tot_arc_cost = tok_cost + l->graph_cost + l->acoustic_cost;
                    Token* next_tok = l->next_tok;
                    if (tot_arc_cost < next_tok->tot_cost) {
                        changed = true;
                        next_tok->tot_cost = tot_arc_cost;
                        next_tok->backpointer = tok;
                    }
                }
            }
        }
    }
}

template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::StrictPrune() {
    int32 curr_frame = this->active_toks_.size() - 1;

    int32 new_last_pruned_frame = curr_frame - prune_offset_;

    BaseFloat final_graph_cost;
    BestPathIterator iter = BestPathEnd(false, &final_graph_cost);
    Token* best_tok = static_cast<Token*>(iter.tok);
    int32 frames_done = 0;  // counting frames processed (instead of pruned)
    Token* best_tok_prev = best_tok->backpointer;
    std::vector<Token*> toks_tosave;
    toks_tosave.reserve(20);

    int32 cnt_frame_to_startprune = curr_frame - new_last_pruned_frame;
    // One iteration through the while loop will prune
    // the tokens of frame f and links (of the best token) of f-1.
    int32 cnt_olabels_on_pruneframes = 0;
    // std::vector<int32> tids;
    while ((curr_frame > this->last_pruned_frame) && best_tok_prev != NULL) {
        bool is_eps = false;
        ForwardLinkT* link;

        if (frames_done == cnt_frame_to_startprune - 1) {

            // Leaving multiple forward links on last (sorted by time) pruned frame.
            // But have to prune eps links that are not part of the best path as
            // their tokens will be pruned.
            ForwardLinkT* prev_link = NULL;
            for (link = best_tok_prev->links; link != NULL; ) {
                if ((link->next_tok != best_tok) && link->ilabel == 0) {
                    ForwardLinkT* next_link = link->next;
                    if (prev_link != NULL) prev_link->next = next_link;
                    else best_tok_prev->links = next_link;
                    delete link;
                    link = next_link;
                } else {
                    if (link->ilabel == 0) is_eps = true;
                    prev_link = link;
                    link = link->next;
                }

            }

        } else if (frames_done >= cnt_frame_to_startprune) {
            ForwardLinkT* prev_link = NULL;
            BaseFloat tok_cost = best_tok_prev->tot_cost;
            bool found_olabel = false;
            for (link = best_tok_prev->links; link != NULL; ) {
                BaseFloat tot_arc_cost = tok_cost + link->graph_cost + link->acoustic_cost;
                // Can be two arcs between two tokens with different olabels so must check cost
                if (link->next_tok != best_tok || !ApproxEqual(tot_arc_cost, best_tok->tot_cost)) {
                    ForwardLinkT* next_link = link->next;
                    if (prev_link != NULL) prev_link->next = next_link;
                    else best_tok_prev->links = next_link;
                    delete link;

                    link = next_link;
                } else {
                    if (link->ilabel == 0) {
                      is_eps = true;
                    } else {
                      ;
                      // tids.push_back(link->ilabel);
                    }
                    if (link->olabel != 0) {
                      found_olabel = true;
                    }
                    prev_link = link;
                    link = link->next;
                }
            }
            if (found_olabel) cnt_olabels_on_pruneframes++;

        } else {
            for (link = best_tok_prev->links; link != NULL; ) {
                if (link->next_tok != best_tok) {
                    ForwardLinkT* next_link = link->next;
                    link = next_link;
                } else {
                    if (link->ilabel == 0) is_eps = true;
                    link = link->next;
                }
            }
        }

        if (is_eps == true) {
            toks_tosave.push_back(best_tok);
        }

        if ((frames_done < cnt_frame_to_startprune) && (!is_eps)) {
            curr_frame--;
            frames_done++;
            toks_tosave.clear();
        } else if (!is_eps) {
            int16 num2save = toks_tosave.size();
            bool foundbest = false;
            if (num2save > 0) {
                toks_tosave.push_back(best_tok);
                Token* prev_tok = NULL;
                Token* next_tok;
                for (Token* tok = this->active_toks_[curr_frame].toks; tok != NULL;) {
                    Token* t;
                    bool should_del = true;
                    for (int16 i = 0; i < num2save + 1; i++) {
                        t = toks_tosave[i];
                        if (t == tok) {
                            should_del = false;
                            foundbest = true;
                            break;
                        }
                    }
                    next_tok = tok->next;
                    if (should_del == true) {
                        this->DeleteForwardLinks(tok);
                        delete tok;
                        num_toks_--;
                        if (prev_tok == NULL) {
                            this->active_toks_[curr_frame].toks = next_tok;
                        } else {
                            prev_tok->next = next_tok;
                        }
                    } else {
                        prev_tok = tok;
                    }
                    tok = next_tok;
                }
                if (!foundbest) {
                    KALDI_ERR << "Didn't match best token in list. Multiple. Error in code!";
                }
                toks_tosave.clear();
            } else {
                Token* prev_tok = NULL;
                Token* next_tok;
                for (Token* tok = this->active_toks_[curr_frame].toks; tok != NULL;) {
                    next_tok = tok->next;
                    if (tok != best_tok) {
                        this->DeleteForwardLinks(tok);
                        delete tok;
                        num_toks_--;
                        if (prev_tok == NULL) {
                            this->active_toks_[curr_frame].toks = next_tok;
                        } else {
                            prev_tok->next = next_tok;
                        }
                    } else {
                        foundbest = true;
                        prev_tok = tok;
                    }
                    tok = next_tok;
                }
                if (!foundbest) {
                    KALDI_ERR << "Didn't match best token in list. Single. Error in code!";
                }
            }
            curr_frame--;
            frames_done++;
        }
        best_tok = best_tok_prev;
        best_tok_prev = best_tok_prev->backpointer;

    }  // end while
    // std::reverse(tids.begin(), tids.end());
    // std::string str = "TRANS IDS PRUNE ";
    // for(int32 i=0;i < tids.size();i++) str += std::to_string(tids[i]) + " ";
    // KALDI_LOG << str;

    this->last_num_states_ += cnt_olabels_on_pruneframes;

    RecalcTotCosts(this->last_pruned_frame - 1);

    this->last_pruned_frame = new_last_pruned_frame;

    PruneTokensAfterRescore();

}

template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::AdjustCostsWithClatSimple(CompactLattice* clat_bestpath) {

    std::vector<int32> trans_ids;
    {
        int32 numstates = clat_bestpath->NumStates();
        std::vector<int32> v;
        v.reserve(5 * numstates);  // should need at least that..
        for(int32 i = 0; i < numstates; i++) {
            fst::ArcIterator<CompactLattice> aiter(*clat_bestpath, i);
            for(; !aiter.Done(); aiter.Next()) {
                const CompactLatticeArc& arc = aiter.Value();
                v = arc.weight.String();
                trans_ids.reserve(trans_ids.size() + v.size());
                trans_ids.insert(trans_ids.end(), v.begin(), v.end());
            }
        }
        v = clat_bestpath->Final(numstates - 1).String();
        trans_ids.reserve(trans_ids.size() + v.size());
        trans_ids.insert(trans_ids.end(), v.begin(), v.end());
    }
    trans_ids.erase(trans_ids.begin(), trans_ids.begin() + this->last_pruned_frame);

    int32 last_frame = this->active_toks_.size() - 1;

    KALDI_ASSERT(trans_ids.size() == last_frame - this->last_pruned_frame);
    int32 modboost_frame_switch = this->last_pruned_frame + prune_offset_;
    int32 curr_frame = this->last_pruned_frame;
    int32 idx_id = 0;
    int32 c_tid = trans_ids[0];  // correct transition id
    bool found_tid;
    BaseFloat mod, max_mod;
    int32 sz_toks2mod;
    Token* curtok;
    Token* prevtok;
    std::pair<Token*, BaseFloat> tok_n_prob;
    std::tuple<Token*, Token*, BaseFloat> toktpl;
    //  tokens that should have tot_cost modified
    //  contains prev token, next token, tot_cost adjustment for next token
    std::vector<std::tuple<Token*, Token*, BaseFloat>> toks2mod;
    while(curr_frame < last_frame) {
        found_tid = false;


        for(Token* tok = this->active_toks_[curr_frame].toks; tok != NULL; tok = tok->next) {
            BaseFloat cur_tok_cost = tok->tot_cost;

            for(ForwardLinkT* l = tok->links; l != NULL; l = l->next) {

                if(l->ilabel == c_tid) {
                    found_tid = true;
                    if (l->olabel == 0) continue;

                    BaseFloat gcost = l->graph_cost;
                    BaseFloat next_tok_cost = l->next_tok->tot_cost;
                    BaseFloat acost = l->acoustic_cost;
                    BaseFloat tot_arc_cost = cur_tok_cost + gcost + acost;
                    BaseFloat factor = 10.0;
                    //if (curr_frame < modboost_frame_switch) {
                    //  l->graph_cost = gcost - factor;
                    //}
                    if (ApproxEqual(tot_arc_cost, next_tok_cost)) {
                        mod = 0.5;
                        if (curr_frame < modboost_frame_switch) {
                            mod *= factor;
                        }
                        l->graph_cost = gcost - mod;
                        std::tuple<Token*, Token*, BaseFloat> toktpl_(tok, l->next_tok, mod);
                        toks2mod.push_back(toktpl_);
                    } else if (tot_arc_cost > next_tok_cost) {
                        mod = 0.5;
                        if (curr_frame < modboost_frame_switch && cur_tok_cost < 10000.0) {
                            mod *= factor;
                            // adding gcost so if between two arcs better one remains better
                            l->graph_cost = next_tok_cost - cur_tok_cost - acost - mod + gcost / 0.1;
                            std::tuple<Token*, Token*, BaseFloat> toktpl_(
                                tok, l->next_tok, mod);
                            toks2mod.push_back(toktpl_);
                        } else {
                            l->graph_cost = gcost - mod;
                            if (tot_arc_cost - mod < next_tok_cost) {
                                std::tuple<Token*, Token*, BaseFloat> toktpl_(tok,
                                    l->next_tok,
                                    next_tok_cost - tot_arc_cost + mod);
                                toks2mod.push_back(toktpl_);
                            } else {
                                BaseFloat tmp = 0.0;
                                std::tuple<Token*, Token*, BaseFloat> toktpl_(
                                    NULL, l->next_tok, tmp);
                                toks2mod.push_back(toktpl_);
                            }
                        }
                    } else if (tot_arc_cost < next_tok_cost) {
                        mod = 0.5;
                        if (curr_frame < modboost_frame_switch) {
                            mod *= factor;
                        }
                        l->graph_cost = gcost - mod;
                        mod = next_tok_cost - tot_arc_cost + mod;
                        std::tuple<Token*, Token*, BaseFloat> toktpl_(tok, l->next_tok,
                            mod);
                        toks2mod.push_back(toktpl_);
                    }
                } // end ilabel == c_tid if
            }
        } // end token loop

        sz_toks2mod = toks2mod.size();
        // Adjust tot_cost of tokens by mod

        if (sz_toks2mod > 0) {

            std::sort(toks2mod.begin(), toks2mod.end(), [](
                    std::tuple<Token*, Token*, BaseFloat> a,
                    std::tuple<Token*, Token*, BaseFloat> b) {
                return std::get<1>(a) < std::get<1>(b);
            });
            prevtok = std::get<1>(toks2mod[0]);  // prevtok is NOT for token of previous frame..
            max_mod = 0.0;
            for(int32 i=0; i <= sz_toks2mod; i++) {
                if (i != sz_toks2mod) {
                    toktpl = toks2mod[i];
                    curtok = std::get<1>(toktpl);
                } else {
                    curtok = NULL;
                }
                if (curtok != prevtok) {
                    prevtok->tot_cost = prevtok->tot_cost - tok_n_prob.second;
                    max_mod = 0.0;
                    if (tok_n_prob.first != NULL) {
                        prevtok->backpointer = tok_n_prob.first;
                    }
                }
                if (i != sz_toks2mod) {
                    mod = std::get<2>(toktpl);
                    if (mod > max_mod) {
                        tok_n_prob.first = std::get<0>(toktpl);
                        tok_n_prob.second = mod;
                        max_mod = mod;
                    }
                }
                prevtok = curtok;
            }
            toks2mod.clear();
        }

        if (found_tid != true) {
            KALDI_ERR << "Couldn't find ilabel " << " on frame " << curr_frame;
        }

        c_tid = trans_ids[++idx_id];
        curr_frame++;
    } // end while

    RecalcTotCosts(this->last_pruned_frame - 1);
}

template <typename FST>
bool LatticeFasterOnlineDecoderTpl<FST>::GetNextTokWrapWithRightLabel(TokW* tokw,
  int32 c_tid, TokW*& next_tokw, std::unordered_map<Token*, TokW*>* tokmap,
  int32 rolabel) {

  if (tokw->worked == 0) return false;

  //KALDI_LOG << "right ilabels " << last_tid << " " <<next_tid;
  if (tokw->worked == 1 && tokw->worked_final == true) {
      next_tokw = tokw->rnext;
      next_tokw->clink = tokw->rlink;
      next_tokw->prev = tokw;
      tokw->next = next_tokw;
      return true;
  }

  bool is_label_fail = false;
  int16 altcnt = tokw->altcnt;
  int16 cnt_valid_flinks = 0;
  Token* nexttok;
  ForwardLinkT* link = NULL;
  if (altcnt != 0) {
    //Timer timer;

    std::vector<bool>& idx_links_used = tokw->idx_links_used;
    std::vector<ForwardLinkT*>& v_links = tokw->v_links;
    int16 num_links = static_cast<int16>(v_links.size());
    int16 idxused;
    for(int16 j = 0; j < num_links; j++) {
      if (idx_links_used[j] == true) continue;
      ForwardLinkT* l = v_links[j];
      // KALDI_LOG << "flink "<<&(*l) << " "<< l->ilabel << " " << l->olabel << " nexttok " << &(*(l->next_tok));
      int32 ilabel = l->ilabel;
      if (ilabel == 0 || ilabel == c_tid) {
        int32 olabel = l->olabel;
        if (olabel == 0 || olabel == rolabel) {
          cnt_valid_flinks++;
          nexttok = l->next_tok;
          link = l;
          idxused = j;
        } else {
          is_label_fail = true;
        }
      } else {
        idx_links_used[j] = true;
      }
    }
    //double ttt = timer.Elapsed();
    //KALDI_LOG << "fp time " << ttt;
    if (link != NULL) {
      idx_links_used[idxused] = true;
      //timer.Reset();
      auto it = tokmap->find(&(*nexttok));
      bool passed = true;
      if (it != tokmap->end()) {  // found
        TokW* poss_tokw = it->second;
        //KALDI_LOG << "olabel check " << tokw->olabel_cnt << " " << poss_tokw->olabel_cnt;
        if(link->olabel == rolabel) {
          if (tokw->olabel_cnt+1 != poss_tokw->olabel_cnt) {
            passed = false;
            is_label_fail = true;
          }
        } else if (tokw->olabel_cnt != poss_tokw->olabel_cnt) {
          passed = false;
          is_label_fail = true;
        }

        if (passed) {
          next_tokw = poss_tokw;
          next_tokw->prev = tokw;
        }
      } else {
        next_tokw->Create(nexttok, -1, tokw);
        (*tokmap)[&(*nexttok)] = next_tokw;
      }
      if (passed) {
        // -1 because one is used right away (num_used not yet updated)
        tokw->altcnt = cnt_valid_flinks - 1;
        next_tokw->clink = link;
        tokw->next = next_tokw;
        //KALDI_LOG << "found tid, link " << &(*link) << " ilabel " << link->ilabel << " tokw " <<&(*next_tokw);
        //double tt = timer.Elapsed();
        //KALDI_LOG << "pp time " <<tt;
        return true;
      } else if (cnt_valid_flinks > 1) {  // could be alternative links that we still need to check
        next_tokw = tokw;
        tokw->altcnt = cnt_valid_flinks - 1;
        //double tt = timer.Elapsed();
        //KALDI_LOG << "pp time " <<tt;
        return true;
      }
      //double tt = timer.Elapsed();
      //KALDI_LOG << "pp time " <<tt;
    }
  }

  if (tokw->worked == 1) {
    tokw->worked_final = true;
  } else if (cnt_valid_flinks == 0 && is_label_fail == false) {
    tokw->worked = 0;
  }
  return false;
}

template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::TraverseAll(Token* tok,
                                             std::vector<int32>* trans_ids,
                                             std::vector<std::vector<ForwardLinkT*>>* paths,
                                             std::vector<CompactLatticeArc::Label>* v_olabels) {

  TokW* cur_tok = new TokW(tok, -1);
  std::unordered_map<Token*, TokW*> tokmap({{&(*tok), cur_tok}});
  tokmap.reserve(3000);

  int32 idx_tid = 0, idx_path = paths->size(), idx_olabels = 0, rolabel;
  int32 c_tid;
  int32 num_iters = 0;
  int32 num_tids = trans_ids->size() - 1;  // -1 appended to both
  int32 num_olabels = v_olabels->size() - 1;
  //Timer timer;
  while(true) {
    if (cur_tok == NULL) break;
    num_iters++;
    c_tid = (*trans_ids)[idx_tid];
    rolabel = (*v_olabels)[idx_olabels];

    // KALDI_LOG<<"tokw " <<&(*cur_tok)<<" tok "<<&(*(cur_tok->tok)) << " tid "<<std::to_string(c_tid)<< " idx " << idx_tid << " olabel " << rolabel<<" "<<idx_olabels<< " cnt " << cur_tok->olabel_cnt;
    TokW* nexttokw = new TokW();
    //timerb.Reset();

    bool ans = GetNextTokWrapWithRightLabel(cur_tok, c_tid, \
      nexttokw, &tokmap, rolabel);

    //double ttt = timerb.Elapsed();
    //KALDI_LOG << "getn time " << ttt;

    if (ans) {
      if (nexttokw != cur_tok) {  // for when retrying tok
        ForwardLinkT* li = nexttokw->clink;
        if (li->ilabel != 0) idx_tid++;
        if (li->olabel != 0) {
          idx_olabels++;
          if (cur_tok->olabel_cnt == nexttokw->olabel_cnt) {
            nexttokw->olabel_cnt += 1;
          }
        }
      }
      cur_tok = nexttokw;
    } else {
      // KALDI_LOG << "dead tok";
      ForwardLinkT* li = cur_tok->clink;
      if (li != NULL) {
        if (li->ilabel != 0) idx_tid--;
        if (li->olabel != 0) {
          idx_olabels--;
        }
      }
      delete nexttokw;
      cur_tok = cur_tok->prev;
      continue;
    }

    if (idx_tid == num_tids && nexttokw->olabel_cnt == num_olabels) {

      TokW* t_tokw = tokmap[tok]->next;  // first has NULL clink
      paths->push_back(std::vector<ForwardLinkT*>());
      std::vector<ForwardLinkT*>* path = &((*paths)[idx_path]);

      // std::string str = "TRANS IDS FOUND ";
      int32 cnt_tids = 0;
      while(t_tokw != NULL) {
        t_tokw->worked = 1;
        TokW* tokw_next = t_tokw->next;
        if (tokw_next != NULL) {
          t_tokw->rnext = tokw_next;
          t_tokw->rlink = tokw_next->clink;
        }

        ForwardLinkT* l = t_tokw->clink;
        if (l->ilabel != 0) cnt_tids++;
        // if (l->olabel == 0) {
        //   str += std::to_string(l->ilabel) + " ";
        // } else {
        //   str += std::to_string(l->ilabel) + " (" + std::to_string(l->olabel) +") ";
        // }

        path->push_back(l);
        t_tokw = tokw_next;
      }
      // KALDI_LOG << str;

      KALDI_ASSERT(cnt_tids == num_tids);
      idx_path++;
      if(nexttokw->clink->olabel != 0) idx_olabels--;
      if(nexttokw->clink->ilabel != 0) idx_tid--;;
      cur_tok = nexttokw->prev;
      ForwardLinkT* l;
      while(cur_tok->worked_final == true) {
        l = cur_tok->clink;
        if(l->olabel != 0) idx_olabels--;
        if(l->ilabel != 0) idx_tid--;

        cur_tok = cur_tok->prev;
      }
      continue;
    }
  }  // end while
  //KALDI_LOG << "num iters " << num_iters;
  //double tt = timer.Elapsed();
  //KALDI_LOG << "while time " << tt;
  //KALDI_LOG << paths->size();

  typename std::unordered_map<Token*, TokW*>::iterator it = tokmap.begin();
  for(; it != tokmap.end(); it++) {
    TokW* tokw2delete = it->second;
    delete tokw2delete;
  }
}

BaseFloat adjust_cost_decay(int32 cur_frame, int32 prune_offset) {
  BaseFloat frame = (BaseFloat) cur_frame;
  BaseFloat f_offset = (BaseFloat) prune_offset;
  BaseFloat ans = -(1.75 * frame / f_offset) * (1.75 * frame / f_offset)+ 2.0;
  if (ans < 0.0) return 0.0;
  else return ans;
}

template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::AdjustCostsWithClatCorrect(CompactLattice* clat_bestpath) {

    std::vector<int32> trans_ids;
    std::vector<int32> olabels;
    {
        int32 numstates = clat_bestpath->NumStates();
        std::vector<int32> v;
        v.reserve(5 * numstates);  // should need at least that..
        for(int32 i = 0; i < numstates; i++) {
            fst::ArcIterator<CompactLattice> aiter(*clat_bestpath, i);
            for(; !aiter.Done(); aiter.Next()) {
                const CompactLatticeArc& arc = aiter.Value();
                v = arc.weight.String();
                olabels.push_back(arc.olabel);
                trans_ids.reserve(trans_ids.size() + v.size());
                trans_ids.insert(trans_ids.end(), v.begin(), v.end());
            }
        }
        v = clat_bestpath->Final(numstates - 1).String();
        trans_ids.reserve(trans_ids.size() + v.size());
        trans_ids.insert(trans_ids.end(), v.begin(), v.end());
    }
    trans_ids.erase(trans_ids.begin(), trans_ids.begin() + this->last_pruned_frame);
    if (this->last_pruned_frame != 0) {
      int32 num_olabels = olabels.size();
      int32 num_to_erase;
      if (num_olabels < this->last_num_states_) num_to_erase = num_olabels;
      else num_to_erase = this->last_num_states_;

      olabels.erase(olabels.begin(), olabels.begin() + num_to_erase);
    }
    // because will go 1 after end, makes sure no olabel is matched after last olabel
    olabels.push_back(-1);
    trans_ids.push_back(-1);  // for when last word on eps trans on final frame

    int32 last_frame = this->active_toks_.size() - 1;

    /*std::string str = "OLABELS ";
    for(int32 i =0;i<olabels.size();i++) {
      str += std::to_string(olabels[i]) + " ";
    }
    KALDI_LOG << str;
    KALDI_LOG << "\nTOK AND LINK INFO\n";
    int32 tmp = last_pruned_frame - 1;
    if (tmp < 0) tmp = 0;
    for(int32 f = tmp; f <= last_pruned_frame+20; f++) {
      KALDI_LOG << "FRAME " << f;
      for(Token* tok = active_toks_[f].toks; tok != NULL; tok=tok->next) {
        KALDI_LOG << "tok " << &(*tok) << " (frame "<<f<<" adj "<<f-last_pruned_frame<<")" << " tcost " << tok->tot_cost;
        for(ForwardLink* l = tok->links; l != NULL; l=l->next) {
          KALDI_LOG << "link " <<&(*l) << " pilabel " << l->ilabel << " polabel "<<l->olabel << " nextt " <<&(*(l->next_tok));
        }
      }
    }
    KALDI_LOG << "\nTOK AND LINK INFO DONE\n";
    std::string str = "TRANS IDS ";
    for(int32 i =0;i<trans_ids.size();i++) {
      str += std::to_string(trans_ids[i]) + " ";
    }
    KALDI_LOG << str;*/

    KALDI_ASSERT(trans_ids.size() - 1 == last_frame - this->last_pruned_frame);
    int32 curr_frame = this->last_pruned_frame;
    std::vector< std::vector<ForwardLinkT*>> paths;

    for(Token* tok = this->active_toks_[curr_frame].toks; tok != NULL; tok = tok->next) {
      TraverseAll(tok, &trans_ids, &paths, &olabels);
    }
    int32 numpaths = paths.size();
    KALDI_ASSERT(numpaths != 0);
    int32 new_last_pruned_frame = this->last_pruned_frame + prune_offset_;
    std::unordered_map<ForwardLinkT*, BaseFloat> link_mods;
    for(int32 i = 0; i < numpaths; i++) {
      std::vector<ForwardLinkT*>* path = &(paths[i]);
      int32 num_to_modify = path->size() - 30;  // adj cost decay should really take care of this
      curr_frame = this->last_pruned_frame;
      for(int32 j = 0; j < num_to_modify; j++) {
        ForwardLinkT* l = (*path)[j];
        if (l->olabel != 0) {
          auto it = link_mods.find(&(*l));
          if (it == link_mods.end()) {  // not found
            if (curr_frame < new_last_pruned_frame) {
              link_mods[&(*l)] = -10.0;
            } else {
              link_mods[&(*l)] = -adjust_cost_decay(curr_frame - new_last_pruned_frame, prune_offset_);
            }

          }
        }
        if (l->ilabel != 0) {
          curr_frame++;
        }
      }
      curr_frame = this->last_pruned_frame;
    }

    std::unordered_map<ForwardLinkT*, BaseFloat>::iterator it = link_mods.begin();
    for(; it != link_mods.end(); it++) {
      ForwardLinkT* link = it->first;
      link->graph_cost += it->second;
    }

    RecalcTotCosts(this->last_pruned_frame - 1);
}


template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::CreateMockToks() {
  Token* start_tok = new Token(0.0, 0.0, NULL, NULL, NULL);
  std::vector<int32> labels = {1, 2, 3, 4, 5, 6};
  std::vector<int32> numlabels = {2, 3, 2, 3, 2, 3};
  std::vector<Token*> best_path = {start_tok};
  std::vector<int32> olabels = {1000, 1001, 1002, -1};
  Token* prevtok = start_tok;
  Token* nexttok;

  // Correct paths
  /*
  1 1 0 2 2 2 3 3 0 4 4 4 5 5 0 6 6 6
  1 1 2 2 2 3 3 0 4 4 4 5 5 0 6 6 6
  1 1 0 2 2 2 3 3 4 4 4 5 5 0 6 6 6

  1 1 0 2 2 2 3 3 0 3 3 4 5 5 0 6 6 6
  1 1 0 2 2 2 3 4 4 4 4 5 5 0 6 6 6
  1 1 0 2 2 2 3 3 3 3 4 5 5 0 6 6 6

  */
  int32 idx = 0;
  for(int32 i=0; i<labels.size(); i++) {
    int32 il = labels[i];
    if( (i+1) % 2 == 0) {
      nexttok = new Token(0.0, 0.0, NULL, NULL, NULL);
      best_path.push_back(nexttok);
      prevtok->links = new ForwardLinkT(nexttok, 0, 0, 0.0, 0.0, prevtok->links);
      KALDI_LOG << "tok " << &(*prevtok) << " ilabel " << prevtok->links->ilabel << " olabel "<<prevtok->links->olabel << " idx " <<idx;
      prevtok = nexttok;
      idx++;
    }
    //KALDI_LOG << "tok " << &(*prevtok) << " ilabel 0 olabel "<<prevtok->links->olabel;
    for(int32 j=0; j<numlabels[i]; j++) {
      nexttok = new Token(0.0, 0.0, NULL, NULL, NULL);
      best_path.push_back(nexttok);
      if (i == 1 && j ==1) {
        prevtok->links = new ForwardLinkT(nexttok, il, 1000, 0.0, 0.0, prevtok->links);
      } else if (i == 3 && j == 0) {
        prevtok->links = new ForwardLinkT(nexttok, il, 1001, 0.0, 0.0, prevtok->links);
      } else if (i == 5 && j == 0) {
        prevtok->links = new ForwardLinkT(nexttok, il, 1002, 0.0, 0.0, prevtok->links);
      } else {
        prevtok->links = new ForwardLinkT(nexttok, il, 0, 0.0, 0.0, prevtok->links);
      }
      KALDI_LOG << "tok " << &(*prevtok) << " ilabel " << prevtok->links->ilabel << " olabel "<<prevtok->links->olabel << " idx " <<idx;
      prevtok = nexttok;
      idx++;
    }
  }

  std::vector<int32> trans_ids;
  for(int32 i=0; i<best_path.size() - 1; i++) {
    int32 il = best_path[i]->links->ilabel;
    if (il != 0) trans_ids.push_back(il);
  }
  trans_ids.push_back(-1);

  std::string str = "Trans ids ";
  for(int32 i=0; i<trans_ids.size(); i++) {
    str += std::to_string(trans_ids[i]) + " ";
  }
  KALDI_LOG << str;

  int32 sz_path = best_path.size();
  Token* tok = best_path[sz_path - 4];
  Token* nexttokm = new Token(0.0, 0.0, NULL, NULL, NULL);
  tok->links = new ForwardLinkT(nexttokm, 100, 0, 0.0, 0.0, tok->links);
  nexttokm->links = new ForwardLinkT(best_path[sz_path-3], 5, 0, 0.0, 0.0, nexttokm->links);

  Token* tokb = best_path[6];
  Token* nexttokb = new Token(0.0, 0.0, NULL, NULL, NULL);
  tokb->links = new ForwardLinkT(nexttokb, 3, 0, 0.0, 0.0, tokb->links);
  Token* nexttokc = new Token(0.0, 0.0, NULL, NULL, NULL);
  nexttokb->links = new ForwardLinkT(nexttokc, 4, 0, 0.0, 0.0, nexttokb->links);

  Token* nexttokd = new Token(0.0, 0.0, NULL, NULL, NULL);
  tokb->links = new ForwardLinkT(nexttokd, 3, 0, 0.0, 0.0, tokb->links);
  Token* nexttoke = new Token(0.0, 0.0, NULL, NULL, NULL);
  nexttokd->links = new ForwardLinkT(nexttoke, 3, 0, 0.0, 0.0, nexttokd->links);
  nexttoke->links = new ForwardLinkT(best_path[10], 4, 1001, 0.0, 0.0, nexttoke->links);

  Token* tokc = best_path[5];
  Token* nexttokf = new Token(0.0, 0.0, NULL, NULL, NULL);
  tokc->links = new ForwardLinkT(nexttokf, 2, 0, 0.0, 0.0, tokc->links);
  Token* nexttokg = new Token(0.0, 0.0, NULL, NULL, NULL);
  nexttokf->links = new ForwardLinkT(nexttokg, 100, 0, 0.0, 0.0, nexttokf->links);

  Token* tokd = best_path[1];
  Token* nexttokh = new Token(0.0, 0.0, NULL, NULL, NULL);
  tokd->links = new ForwardLinkT(nexttokh, 1, 0, 0.0, 0.0, tokd->links);
  nexttokh->links = new ForwardLinkT(best_path[4], 2, 0, 0.0, 0.0, nexttokh->links);

  // Creating connection so ilabel path goes 1 1 2 2 2 3 4 4 4 4 ..
  Token* toke = best_path[7];
  toke->links = new ForwardLinkT(best_path[9], 4, 0, 0.0, 0.0, toke->links);

  // Creating connection so ilabel path goes .. 2 2 2 3 3 3 3 4 5 ..
  Token* tokf = best_path[7];
  Token* nexttokz = new Token(0.0, 0.0, NULL, NULL, NULL);
  tokf->links = new ForwardLinkT(nexttokz, 3, 0, 0.0, 0.0, tokf->links);
  Token* nexttokx = new Token(0.0, 0.0, NULL, NULL, NULL);
  nexttokz->links = new ForwardLinkT(nexttokx, 3, 0, 0.0, 0.0, nexttokz->links);
  Token* nexttoky = new Token(0.0, 0.0, NULL, NULL, NULL);
  nexttokx->links = new ForwardLinkT(nexttoky, 3, 0, 0.0, 0.0, nexttokx->links);
  nexttoky->links = new ForwardLinkT(best_path[12], 4, 1001, 0.0, 0.0, nexttoky->links);

  // Creating connections without last word ( .. 6 6 6), same tid list
  best_path[15]->links = new ForwardLinkT(best_path[16], 6, 0, 0.0, 0.0, best_path[15]->links);

  // Creating connections with extra word at end ( .. 6 6 6), same tid list
  best_path[16]->links = new ForwardLinkT(best_path[17], 6, 1003, 0.0, 0.0, best_path[16]->links);

  // alternative path going 3 0 4, should fail on 4, to see whether ilabel history
  // is updated correctly (when going back), has to pick up another path
  // with ilabel 3
  Token* tokm = best_path[9];
  Token* nexttokaa = new Token(0.0, 0.0, NULL, NULL, NULL);
  tokm->links = new ForwardLinkT(nexttokaa, 4, 0, 0.0, 0.0, tokm->links);
  tokm->links = new ForwardLinkT(nexttokx, 3, 0, 0.0, 0.0, tokm->links);

  // Path where final word is on eps transition on final frame
  Token* atok = best_path[15];
  Token* btok = new Token(0.0, 0.0, NULL, NULL, NULL);
  atok->links = new ForwardLinkT(btok, 6, 0, 0.0, 0.0, atok->links);
  Token* ctok = new Token(0.0, 0.0, NULL, NULL, NULL);
  btok->links = new ForwardLinkT(ctok, 6, 0, 0.0, 0.0, btok->links);
  Token* dtok = new Token(0.0, 0.0, NULL, NULL, NULL);
  ctok->links = new ForwardLinkT(dtok, 6, 0, 0.0, 0.0, ctok->links);
  Token* etok = new Token(0.0, 0.0, NULL, NULL, NULL);
  dtok->links = new ForwardLinkT(etok, 0, 1002, 0.0, 0.0, dtok->links);
  Token* ftok = new Token(0.0, 0.0, NULL, NULL, NULL);
  dtok->links = new ForwardLinkT(ftok, 20, 1002, 0.0, 0.0, dtok->links);  // bad link

  // TODO: this is to check that when getting to a working path all the right links are used.
  // Was a bug where when another path had joined with a working path a link would
  // be pointing to the joinee, which results in a joinee further up the path eventually
  // getting a wrong link when going down that path


  std::vector< std::vector<ForwardLinkT*>> paths;
  TraverseAll(start_tok, &trans_ids, &paths, &olabels);
  KALDI_LOG << "paths found " <<paths.size();

}

template <typename FST>
void LatticeFasterOnlineDecoderTpl<FST>::RunMockTraverse() {
  CreateMockToks();
}

// Instantiate the template for the FST types that we'll need.
template class LatticeFasterOnlineDecoderTpl<fst::Fst<fst::StdArc> >;
template class LatticeFasterOnlineDecoderTpl<fst::VectorFst<fst::StdArc> >;
template class LatticeFasterOnlineDecoderTpl<fst::ConstFst<fst::StdArc> >;
template class LatticeFasterOnlineDecoderTpl<fst::GrammarFst>;



} // end namespace kaldi.
