#include "qnnpack.h"
#include "util/kaldi-io.h"
#include "matrix/kaldi-matrix.h"
#include "cudamatrix/kaldi-qmat.h"
#include "cudamatrix/cu-matrix.h"
#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include <random>
#include <functional>
#include "util/parse-options.h"
 
using namespace kaldi;

int run_quant(std::string inmatf, std::string wmatf) {

    bool binary = false;
    CuMatrix<BaseFloat> inmat;
    ReadKaldiObject(inmatf, &inmat);
    inmat.Row(84997).SetZero();

    CuMatrix<BaseFloat> wmat_cu;
    ReadKaldiObject(wmatf, &wmat_cu);

    KALDI_LOG << "in " << inmat.NumRows() << "x "<<inmat.NumCols() << "max " << inmat.Max();
    for(int32 i = 0; i < inmat.NumRows(); i++) {
        for(int32 j = 0; j < inmat.NumCols(); j++) {
            BaseFloat val = inmat(i,j);
            if(val > 10.0f)
                KALDI_LOG << val << " " <<i<<" "<< j;
        }
    }
    KALDI_LOG << "weight " << wmat_cu.NumRows() << "x"<<wmat_cu.NumCols();
    size_t batch_sz = inmat.NumRows();
    size_t in_sz = inmat.NumCols();
    size_t out_sz = wmat_cu.NumRows();

    QMat imat;
    imat.SetQuantiseFromRealMat(&inmat);
    CuMatrix<BaseFloat> tmpin(batch_sz, in_sz);
    imat.DeQuantiseAddToRealMat(&tmpin);

    QMat wmat;
    KALDI_LOG << "kernel";
    wmat.SetQuantiseFromRealMat(&wmat_cu);

    QMat omat;
    omat.Resize(batch_sz, out_sz);

    // std::vector<int32> bias(out_sz, 0);
    // calculate real output
    // Matrix<BaseFloat> realout(batch_sz, out_sz);
    // realout.CopyRowsFromVec(bvec);
    // realout.AddMatMat(1.0, inmat, kNoTrans, kmat, kTrans, 1.0);
    // Vector<BaseFloat> real_means(out_sz);
    // for(int32 i = 0; i < out_sz; i++) {
    //     SubMatrix<BaseFloat> col(realout, 0, batch_sz, i, 1);
    //     real_means(i) = col.Sum() / batch_sz;
    //     // KALDI_LOG << real_means(i) << " "<<col.Min()<<" "<<col.Max();
    // }
    // KALDI_LOG << inmat.Min() << " "<<inmat.Max();
    // KALDI_LOG << kmat.Min() << " "<<kmat.Max();

    // finding output range
    Matrix<BaseFloat> acc(batch_sz, out_sz);
    // for (int32 i = 0; i < batch_sz; i++) {
    //     for (size_t oc = 0; oc < out_sz; oc++) {
    //         acc(i, oc) = bmat.Data()[oc];
    //     }
    // }
    // std::vector<double> acc(batch_sz * out_sz, 0);
    for (int32 i = 0; i < batch_sz; i++) {
        for (size_t oc = 0; oc < out_sz; oc++) {
            for (size_t ic = 0; ic < in_sz; ic++) {
                //std::cerr << int(imat.Data()[i * in_sz + ic]) << " " << int(wmat.Data()[oc * in_sz + ic]) << std::endl;
                acc(i, oc) +=
                    float(imat.scale) * float(wmat.scale) * 
                    float((int32_t(imat.Data()[i * in_sz + ic]) - int32_t(imat.zeropoint)) *
                    (int32_t(wmat.Data()[oc * in_sz + ic]) - int32_t(wmat.zeropoint)));
            }
            //std::cerr << i << " " <<oc << " " << acc[i*out_sz + oc] << " ";
        }
    }
    KALDI_LOG << acc(1, 22);
    KALDI_LOG << "output ";
    omat.SetQuantiseFromRealMat(&acc);
    KALDI_LOG  << omat.minval_ << " " << omat.maxval_;
    // done finding range
    
    // adjusting biases
    // Vector<BaseFloat> qmeans(out_sz);
    // for(int32 i = 0; i < out_sz; i++) {
    //     SubMatrix<BaseFloat> col(acc, 0, batch_sz, i, 1);
    //     BaseFloat qoutmean = col.Sum() / batch_sz;
    //     // want qoutmean + bvec(i) to equal real_means(i)
    //     BaseFloat oldb = bvec(i);
    //     bvec(i) = real_means(i) - qoutmean;
    //     // KALDI_LOG << "realmean " << real_means(i) << " qmean " << qoutmean+oldb << " oldb " << oldb << " newb "<<bvec(i);
    //     // KALDI_LOG << real_means(i) << " "<<col.Min()<<" "<<col.Max();
    // }
    // done adjusting biases

    qnnp_operator_t op = NULL;
    qnnp_status initStatus = qnnp_initialize();
    if (!initStatus == qnnp_status_success) {
        std::cerr << "Failed create";
        return 1;
    }
    std::vector<int32> bias(out_sz, 0);
    qnnp_status createStatus = qnnp_create_fully_connected_nc_q8(
            in_sz,
            out_sz,
            imat.zeropoint,
            imat.scale,
            wmat.zeropoint,
            wmat.scale,
            wmat.Data(),
            bias.data(),
            omat.zeropoint, omat.scale,
            std::numeric_limits<uint8_t>::min(),
            std::numeric_limits<uint8_t>::max(),
            0,
            &op);

    if (!createStatus == qnnp_status_success) {
        std::cerr << "Failed create";
        return 1;
    }
    const uint8_t* inputPtr = imat.Data();
    qnnp_status setupStatus = qnnp_setup_fully_connected_nc_q8(op, batch_sz, 
            inputPtr, in_sz, omat.Data(), out_sz);

    if (!setupStatus == qnnp_status_success) {
        std::cerr << "Failed setup";
        return 1;
    }

    const qnnp_status runStatus = qnnp_run_operator(op, NULL);

    if (!runStatus == qnnp_status_success) {
        std::cerr << "Failed run ";
        return 1;
    }

    CuMatrix<BaseFloat> omat_cu(batch_sz, out_sz);
    omat.DeQuantiseAddToRealMat(&omat_cu);

    // Compare to unquantised
    CuMatrix<BaseFloat> out_unquantised(batch_sz, out_sz);
    out_unquantised.AddMatMat(1.0, inmat, kNoTrans, wmat_cu, kTrans, 0.0);

    BaseFloat max_diff = 0.5;
    // for(int i = 0; i < batch_sz; i++) {
    //     for (int j = 0; j < out_sz; j++) {
    //         BaseFloat diff = std::abs(omat_cu(i, j) - out_unquantised(i, j));
    //         if (diff > 0.001) {
    //             KALDI_LOG << i<< " "<<j;
    //         }
    //     }
    // }
    CuMatrix<BaseFloat> diffmat(omat_cu);
    diffmat.AddMat(-1.0, out_unquantised);
    diffmat.ApplyPow(2.0);
    diffmat.ApplyPow(0.5);
    max_diff = diffmat.Max();
    BaseFloat mean = diffmat.Sum() / (batch_sz * out_sz);
    KALDI_LOG << "largest difference with unqantised MM: " << max_diff << " mean " <<mean;
    
    // Compare to fake quantised
    double imax = inmat.Max(), imin = inmat.Min();
    
    inmat.FakeQuantise(imin, imax, &inmat);
    
    double wmax = wmat_cu.Max(), wmin = wmat_cu.Min();
    CuMatrix<BaseFloat> wmat_cu_orig(wmat_cu);
    wmat_cu.FakeQuantise(wmin, wmax, &wmat_cu);

    CuMatrix<BaseFloat> out_quantised(batch_sz, out_sz);
    out_quantised.AddMatMat(1.0, inmat, kNoTrans, wmat_cu, kTrans, 0.0);
    KALDI_LOG << "fakequant val  " << out_quantised(1, 22);
    out_quantised.FakeQuantise(omat.minval_, omat.maxval_, &out_quantised);
    KALDI_LOG << "fakequant val  " << out_quantised(1, 22);
    max_diff = 0.001;
    BaseFloat cnt = 0;
    for(int i = 0; i < batch_sz; i++) {
        for (int j = 0; j < out_sz; j++) {
            BaseFloat diff = std::abs(omat_cu(i, j) - out_quantised(i, j));
            if (diff > max_diff) {
                if (i < 3)
                    KALDI_LOG << i<< " "<<j;
                cnt++;
            }
        }
    }
    KALDI_LOG << cnt << " from " << batch_sz*out_sz<<" over limit.";
    diffmat.CopyFromMat(omat_cu);
    diffmat.AddMat(-1.0, out_quantised);
    diffmat.ApplyPow(2.0);
    diffmat.ApplyPow(0.5);
    max_diff = diffmat.Max();
    mean = diffmat.Sum() / (batch_sz * out_sz);
    KALDI_LOG << "largest difference with quantised MM: " << max_diff << " mean " <<mean;

    // compare unquant to quant
    diffmat.CopyFromMat(out_unquantised);
    diffmat.AddMat(-1.0, out_quantised);
    diffmat.ApplyPow(2.0);
    diffmat.ApplyPow(0.5);
    max_diff = diffmat.Max();
    mean = diffmat.Sum() / (batch_sz * out_sz);
    KALDI_LOG << "largest difference between unquant and quantised MM: " << max_diff << " mean " <<mean;

    CuMatrix<BaseFloat> wmat_quant(out_sz, in_sz);
    wmat.DeQuantiseAddToRealMat(&wmat_quant);
    CuMatrix<BaseFloat> w_diff(wmat_cu);
    w_diff.AddMat(-1.0f, wmat_quant);
    max_diff = w_diff.Max();
    mean = w_diff.Sum() / (w_diff.NumCols() * w_diff.NumRows());
    KALDI_LOG << "weights maxdiff " << max_diff << " mean diff " << mean;

    CuMatrix<BaseFloat> input_quant(batch_sz, in_sz);
    imat.DeQuantiseAddToRealMat(&input_quant);
    CuMatrix<BaseFloat> in_diff(inmat);
    in_diff.AddMat(-1.0f, input_quant);
    max_diff = in_diff.Max();
    mean = in_diff.Sum() / (in_diff.NumCols() * in_diff.NumRows());
    KALDI_LOG << "inputs maxdiff " << max_diff << " mean diff " << mean;
    
    // analyse output
    KALDI_LOG << out_quantised(1, 22);
    KALDI_LOG << omat_cu(1, 22);

    KALDI_LOG << "doing MM " << omat.zeropoint << " " << omat.scale;
    CuVector<BaseFloat> inv(inmat.Row(1));
    CuVector<BaseFloat> w(wmat_cu.Row(22));
    CuVector<BaseFloat> outv(w);
    outv.MulElements(inv);
    float finval = 0.f;
    for (int32 i = 0; i < in_sz; i++) {
        float val = inv(i) * w(i);
        // KALDI_LOG << finval <<" "<<val;
        finval += val;
    }
    KALDI_LOG << "fake q manual " << finval;
    KALDI_LOG << finval / omat.scale << " " <<std::round(finval / omat.scale);
    // KALDI_LOG << "fake q " << val;

    inv.CopyFromVec(input_quant.Row(1));
    CuVector<BaseFloat> w_q(wmat_quant.Row(22));
    outv.CopyFromVec(w);
    outv.MulElements(inv);
    // KALDI_LOG << "real q int " << int(omat(1, 22)) << " " << int(omat.zeropoint);
    // KALDI_LOG << "real q " << outv.Sum();

    // manual MM
    finval = 0.f;
    for (int32 i = 0; i < in_sz; i++) {
        float val = float(imat.scale) * float(wmat.scale) * 
                    float((int32_t(imat(1, i)) - int32_t(imat.zeropoint)) *
                    (int32_t(wmat(22, i)) - int32_t(wmat.zeropoint)));
        // KALDI_LOG << finval <<" "<<val;
        finval += val;
    }
    KALDI_LOG << "real q " << finval;

    // outv.AddMatVec(1.0, wmat_quant, kNoTrans, inv, 1.0);
    // KALDI_LOG << outv(56);
    // KALDI_LOG << "out " <<out_quantised.Row(777);
    // KALDI_LOG << "out " << omat_cu.Row(777);
    // CuVector<BaseFloat> diff(out_quantised.Row(777));
    // diff.AddVec(1.0, omat_cu.Row(777), -1.0);
    // KALDI_LOG << diff;
    // CuVector<BaseFloat> w(wmat_cu.Row(56));
    // CuVector<BaseFloat> inp(inmat.Row(777));
    // inp.MulElements(w);
    // KALDI_LOG << inp.Sum();
    // CuVector<BaseFloat> m(out_sz);
    // m.AddMatVec(1.0, wmat_cu, kNoTrans, inp, 1.0);
    // KALDI_LOG << m(56);
    // KALDI_LOG << m;
    // KALDI_LOG << out_unquantised.Row(4);
    // KALDI_LOG << out_quantised.Row(4);
    // KALDI_LOG << omat_cu.Row(4);
    //Output ko(qkern_matf, false);
    //wmat.Write(ko.Stream(), false);
    //Output kb(qb_vecf, false);
    //bvec.Write(kb.Stream(), false);
    // Output kob("qout.mat.txt", false);
    // omat.Write(kob.Stream(), false);
    return 0;
}

int main(int argc, char* argv[]) {

    const char* usage =
        "Execute FC layer with given input and weight, print output min/max\n"
        "Usage: run_quant_fc <input-mat> <weight-mat> <bias-mat> <quant-weight-mat>\n";

    ParseOptions po(usage);
    po.Read(argc, argv);

    if (po.NumArgs() != 2) {
      po.PrintUsage();
      return 1;
    }
    std::string inmatf = po.GetArg(1);
    std::string wmatf = po.GetArg(2);
    return run_quant(inmatf, wmatf);
}

