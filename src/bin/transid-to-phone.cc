#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "gmm/am-diag-gmm.h"
#include "hmm/transition-model.h"
#include "gmm/mle-am-diag-gmm.h"
#include "tree/build-tree-utils.h"
#include "tree/context-dep.h"
#include "tree/clusterable-classes.h"
#include "util/text-utils.h"
#include "fst/fstlib.h"



namespace kaldi {

  void TransIdToPhone(const std::vector<int32> &trans_id,
                  const TransitionModel &trans_model,
                  const fst::SymbolTable &phones_symtab) {
      std::vector<int32> phones;
      int32 pdf_id = trans_model.TransitionIdToPdf(trans_id[0]);
      int32 ph_id = trans_model.TransitionIdToPhone(trans_id[0]);
      std::vector<int32> pdf_ids(1, pdf_id);
      GetPhonesForPdfs(trans_model, pdf_ids, &phones);
      std::ostringstream ss;
      for (int32 idx = 0; idx < phones.size(); idx ++)
          ss << phones_symtab.Find(phones[idx]) << ' ';
      std::cout << "PDF ID: " << pdf_ids[0] << " phone: " << phones_symtab.Find(ph_id) << " paired phones: " << ss.str() << '\n';
  }

}



int main(int argc, char *argv[]) {
  using namespace kaldi;
  typedef kaldi::int32 int32;

  const char *usage =
    "Get a list of corresponding phones for a trans-id in a decision tree\n"
    "Usage:  trans-to-phone <trans-id> <tree-in> <topo-in> <phone-table>\n"
    "e.g.: \n"
    "  pdf-to-phone 231 tree topo phones.txt\n";


  ParseOptions po(usage);
  po.Read(argc, argv);

  if (po.NumArgs() != 4) {
      po.PrintUsage();
      return 1;
  }

  std::string
    pdf_id = po.GetArg(1),
    tree_filename = po.GetArg(2),
    topo_filename = po.GetArg(3),
    phones_symtab_filename = po.GetArg(4);

  ContextDependency ctx_dep; ReadKaldiObject(tree_filename, &ctx_dep);
  HmmTopology topo; ReadKaldiObject(topo_filename, &topo);
  TransitionModel trans_model(ctx_dep, topo);

  fst::SymbolTable *phones_symtab = NULL;
  {  // read phone symbol table.
    std::ifstream is(phones_symtab_filename.c_str());
      phones_symtab = fst::SymbolTable::ReadText(is, phones_symtab_filename);
      if (!phones_symtab) KALDI_ERR << "Could not read phones symbol-table file "<<phones_symtab_filename;
  }

  std::vector<int32> trans(1, stoi(pdf_id));

  TransIdToPhone(trans, trans_model, *phones_symtab);

}
