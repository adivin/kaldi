#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include <ctime>

int main(int argc, char *argv[]) {
    using namespace kaldi;
    const char* usage =
        "Create new features from existing ones with modified values. Input assumed to be fbank."
        "Usage: augment-feats <feats.ark> <new-feats.ark";

    ParseOptions po(usage);
    bool binary = true;
    po.Register("binary", &binary, "Binary-mode output (not relevant if writing "
                "to archive)");
    po.Read(argc, argv);

    if (po.NumArgs() != 2) {
        po.PrintUsage();
        exit(1);
    }

    std::string rspecifier = po.GetArg(1);
    std::string wspecifier = po.GetArg(2);
    SequentialBaseFloatMatrixReader kaldi_reader(rspecifier);
    BaseFloatMatrixWriter kaldi_writer(wspecifier);
    std::srand (std::time (nullptr));
    for (; !kaldi_reader.Done(); kaldi_reader.Next()) {
        Matrix<BaseFloat>& mat = kaldi_reader.Value();
        BaseFloat mean = mat.Sum() / (mat.NumRows() * mat.NumCols());
        Matrix<BaseFloat> noisemat(mat.NumRows(), mat.NumCols());
        int32 div = RandInt(4, 6);
        BaseFloat noise_sigma = (mean - mat.Min()) / (float) div;
        noisemat.Set(mean);
        noisemat.AddMat(-1.0, mat);
        noisemat.ApplyFloor(0.f);
        Matrix<BaseFloat> gausnoise(mat.NumRows(), mat.NumCols());
        gausnoise.SetRandn();
        gausnoise.Scale(noise_sigma);
        gausnoise.ApplyCeiling(3.0f);
        gausnoise.ApplyFloor(-8.0f);
        noisemat.AddMat(1.0f, gausnoise);
        noisemat.AddMat(1.0, mat);
        kaldi_writer.Write(kaldi_reader.Key(), noisemat);
    }
}
