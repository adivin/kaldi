#include "cudamatrix/kaldi-qmat.h"
#include "util/parse-options.h"
#include "util/kaldi-io.h"


using namespace kaldi;

int main(int argc, char* argv[]) {

    const char* usage = "Read and write quant matrix\n";

    bool inbinary = true;
    bool outbinary = true;

    ParseOptions po(usage);
    po.Register("inbinary", &inbinary, "Input is binary");
    po.Register("outbinary", &outbinary, "Write in binary");

    po.Read(argc, argv);

    if (po.NumArgs() != 2) {
        po.PrintUsage();
        return 1;
    }

    std::string inmatf = po.GetArg(1);
    std::string outmatf = po.GetArg(2);

    QMat qmat;
    Input ki(inmatf, &inbinary);
    qmat.Read(ki.Stream(), inbinary);

    Output ko(outmatf, outbinary);
    qmat.Write(ko.Stream(), outbinary);
}
