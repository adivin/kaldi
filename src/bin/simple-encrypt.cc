#include<stdio.h>
#include<string>

void xorencrypt(char* msg, size_t msglen, char* msg_enc) {
    std::string key = "r44KNz0YDWHI9Ac889qFpqfrBTSV9BVouukienjxL8ILBp03q0L6SsVRrBNWyHXR7UHYtiTGUMShKQxJ9DmPoIIZBvAqlwrwNyBbzDh7lOkEccH7f107BKewIgHWi5n3";
    const char* s = key.c_str();
    size_t keylen = key.size();

    for(int i = 0; i < msglen; ++i) {
        if (i % 5 == 0) {
            msg_enc[i] = msg[i] ^ s[i % keylen - 11];
        } else if (i % 7 == 0) {
            msg_enc[i] = msg[i] ^ s[i % keylen - 23];
        } else if (i % 11 == 0) {
            msg_enc[i] = msg[i] ^ s[i % keylen - 29];
        } else {
            msg_enc[i] = msg[i] ^ s[i % keylen];
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        printf("ERROR: Must pass in two arguments!\n");
        return 1;
    }

    FILE* f = fopen(argv[1], "r");
    int sz;
    fseek(f, 0L, SEEK_END);
    sz = ftell(f);
    rewind(f);
    char text[sz];
    fread(text, 1, sz, f);
    fclose(f);

    char textenc[sz];
    xorencrypt(text, sz, textenc);

    FILE* fout = fopen(argv[2], "wb");
    fwrite(textenc, sz, 1, fout);
    fclose(fout);
    return 0;
}
