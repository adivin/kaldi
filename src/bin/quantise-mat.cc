
#include "util/parse-options.h"
#include "util/kaldi-io.h"
#include "matrix/kaldi-matrix.h"
#include "cudamatrix/kaldi-qmat.h"
#include <algorithm>

using namespace kaldi;

void quantise_file(std::string inmatf, std::string outmatf, bool binary) {

    Matrix<BaseFloat> inmat;
    ReadKaldiObject(inmatf, &inmat);

    QMat qmat;
    qmat.SetQuantiseFromRealMat(&inmat);

    KALDI_LOG << qmat.minval_ << " " << qmat.maxval_;
    KALDI_LOG << "scale " << qmat.scale;
    KALDI_LOG << "zeropoint " << int(qmat.zeropoint);
    CuMatrix<BaseFloat> mat_float(qmat.NumRows(), qmat.NumCols());
    // qmat.DeQuantiseAddToRealMat(&mat_float);
    KALDI_LOG << int(qmat(1, 0));
    Output ko(outmatf, binary);
    qmat.Write(ko.Stream(), binary);
}

int main(int argc, char* argv[]) {
    const char* usage =
        "Quantise given matrix.\n"
        "Usage: quantise-matrix [binary] <in.mat> <out.mat>\n";

    bool binary = false;

    ParseOptions po(usage);

    po.Register("binary", &binary, "Write out in binary");

    po.Read(argc, argv);

    if (po.NumArgs() != 2) {
        po.PrintUsage();
        return 1;
    }

    std::string inmat = po.GetArg(1);
    std::string outmat = po.GetArg(2);
    quantise_file(inmat, outmat, binary);
}
