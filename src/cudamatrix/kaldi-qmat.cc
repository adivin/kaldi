#include "cudamatrix/kaldi-qmat.h"


namespace kaldi {

void QMat::Init(int32 num_rows, int32 num_cols, float s,
        uint8_t zp, double minval) {
  Resize(num_rows, num_cols, true);
  scale = s;
  zeropoint = zp;
  minval_ = minval;
}

void QMat::Init(int32 num_rows, int32 num_cols, double minval,
        double maxval) {
  Resize(num_rows, num_cols, true);
  SetQuantParams(minval, maxval, this);
}

void QMat::Resize(int32 num_rows, int32 num_cols, bool alloc) {
  if (num_rows_ == num_rows && num_cols_ == num_cols && holding_mem) return;
  num_rows_ = num_rows;
  num_cols_ = num_cols;
  num_elem_ = num_rows * num_cols;
  if (holding_mem) {
    delete[] this->data_;
    this->data_ = NULL;
  }
  if (alloc) {
    this->data_ = new uint8_t[num_rows * num_cols];
    holding_mem = true;
    // for(int32 i = 0; i < num_elem_; i++) this->data_[i] = 0;
  }
}

void QMat::Read(std::istream & in, bool binary) {

  if (binary) {
    const char* my_token = "QQ";

    std::string token;
    ReadToken(in, binary, &token);
    if (token != my_token) {
      KALDI_ERR << "Got token " << token << " when expecting QQ";
      return;
    }

    int32 rows, cols;
    ReadBasicType(in, binary, &rows);
    ReadBasicType(in, binary, &cols);
    ReadBasicType(in, binary, &zeropoint);
    ReadBasicType(in, binary, &scale);

    Resize(rows, cols);
    in.read(reinterpret_cast<char*>(Data()), rows * cols);
    if (in.fail()) {
      KALDI_ERR << "Failed to read QMat!";
    }
  } else {
    std::string zp;  // zeropoint
    in >> zp;
    std::string scale_str;
    in >> scale_str;
    zeropoint = static_cast<uint8_t>(std::stoi(zp));
    scale = std::stof(scale_str);
    in >> zp;  // for [ 
    
    std::vector<std::vector<uint8_t>* > data;
    std::vector<uint8_t>* cur_row = new std::vector<uint8>;
    while(1) {
      int i = in.peek();
      if (i == -1) {
        KALDI_ERR << "Got EOF while reading uint8 mat";
        return;
      }
      if (static_cast<char>(i) == ']') {
        in.get();  // for ]
        in.get();  // for \n

        if (!cur_row->empty()) data.push_back(cur_row);
        else delete(cur_row);

        if (data.empty()) {
          KALDI_ERR << "No data, failed to read uint8 mat";
          return;
        }
        int32 num_rows = data.size(), num_cols = data[0]->size();
        this->Resize(num_rows, num_cols);
        for(int32 i=0; i<num_rows; i++) {
          if (static_cast<int32>(data[i]->size()) != num_cols) {
            KALDI_ERR << "Inconsistent number of cols";
            return;
          }
          for(int32 j = 0; j < num_cols; j++) {
            uint8_t val = (*(data[i]))[j];
            this->Data()[i*num_cols + j] = val;
          }
          delete data[i];
          data[i] = NULL;
        }
        return;
      } else if (static_cast<char>(i) == '\n') {
        in.get();
        if (cur_row->size() != 0) {
          data.push_back(cur_row);
          cur_row = new std::vector<uint8>;
          cur_row->reserve(data.back()->size());
        }
      } else if (i >= '0' && i<='9') {
        uint8_t n;
        std::string s;
        in >> s;
        n = static_cast<uint8_t>(std::stoi(s));
        if (in.fail()) {
          KALDI_ERR << "Couldn't read number in uint8 mat";
          return;
        }
        cur_row->push_back(n);
      } else if (isspace(i)) {
        in.get();
      } else {
        std::string str;
        in >> str;
        KALDI_ERR << "Something's wrong: " << str;
        return;
      }
    }
  }
}

void QMat::Write(std::ostream & out, bool binary) const {
  if (!out.good()) {
    KALDI_ERR << "Stream not good for QMat write.";
    return;
  }
  if (binary) {
    std::string my_token = "QQ";

    WriteToken(out, binary, my_token);
    
    WriteBasicType(out, binary, this->NumRows());
    WriteBasicType(out, binary, this->NumCols());
    WriteBasicType(out, binary, zeropoint);
    WriteBasicType(out, binary, scale);

    out.write(reinterpret_cast<const char*>(cData()), 
      static_cast<size_t>(num_rows_) * static_cast<size_t>(num_cols_));

    if (!out.good()) {
      KALDI_ERR << "Failed to write QMat.";
    }
  } else {
    out << std::to_string(zeropoint) << " " << scale << " [";
    for (int32 i = 0; i < num_rows_; i++) {
      out << "\n";
      for (int32 j = 0; j < num_cols_; j++) {
        out << std::to_string(this->cData()[i * num_cols_ +j]) << " ";
      }
    }
    out << "]\n";
  }
}

inline uint8_t QuantiseVal(BaseFloat v, float s, uint8_t zeropoint) {
  float ratio = v / s;
  // if (ratio < 0.5f && ratio > 0.49f) ratio = 1.0f;
  // if (ratio > -0.5f && ratio < -0.49f) ratio = -1.0f;
  int32 trans_val = static_cast<int32>(std::round(ratio) +
                                        int32(zeropoint));
  int32 clamped = std::max(0, std::min(255, trans_val));
  return static_cast<uint8_t>(clamped);
}

void QMat::SetQuantiseFromRealMat(const MatrixBase<BaseFloat>* M) {
  const int32 num_rows = M->NumRows(),
    num_cols = M->NumCols();
  this->Resize(num_rows, num_cols, true);
  
  double minval = M->Min(),
    maxval = M->Max();

  SetQuantParams(minval, maxval, this);
  
  for(int32 i = 0; i < num_rows_; i++) {
    for(int32 j = 0; j < num_cols_; j++) {
      BaseFloat val = (*M)(i, j);
      this->Data()[i * num_cols_ + j] = QuantiseVal(val, this->scale, this->zeropoint);
    }
  }
}

void QMat::SetQuantiseFromRealMat(const CuMatrixBase<BaseFloat>* M) {
  const int32 num_rows = M->NumRows(),
    num_cols = M->NumCols();
  this->Resize(num_rows, num_cols, true);
  
  double minval = M->Min(),
    maxval = M->Max();

  SetQuantParams(minval, maxval, this);
  
  for(int32 i = 0; i < num_rows_; i++) {
    for(int32 j = 0; j < num_cols_; j++) {
      BaseFloat val = (*M)(i, j);
      this->Data()[i * num_cols_ + j] = QuantiseVal(val, this->scale, this->zeropoint);
    }
  } 
}

void QMat::SetQuantiseFromRealMatCentile(const CuMatrixBase<BaseFloat>* M, 
                                   BaseFloat perc) {
  const int32 num_rows = M->NumRows(),
    num_cols = M->NumCols();
  this->Resize(num_rows, num_cols, true);
  double minv, maxv;
  M->FindCentiles(perc, &minv, &maxv);
  SetQuantParams(minv, maxv, this);
  
  for(int32 i = 0; i < num_rows_; i++) {
    for(int32 j = 0; j < num_cols_; j++) {
      BaseFloat val = (*M)(i, j);
      this->Data()[i * num_cols_ + j] = QuantiseVal(val, this->scale, this->zeropoint);
    }
  }
}

void QMat::QuantiseRealMat(const CuMatrixBase<BaseFloat>* M) {
  this->Resize(M->NumRows(), M->NumCols());
  int32 stride = M->Stride();
  float reciprocal_scale = 1 / this->scale;
  for(int32 i = 0; i < num_rows_; i++) {
    int32 src_offset = i * stride;
    int32 dst_offset = i * num_cols_;
    for(int32 j = 0; j < num_cols_; j++) {
      // BaseFloat val = M->Data()[src_offset + j];
      // this->Data()[dst_offset + j] = QuantiseVal(val, this->scale, this->zeropoint);
      int32 tmp = std::round(reciprocal_scale * M->Data()[src_offset + j]) + int32(zeropoint);
      this->Data()[dst_offset + j] = static_cast<uint8_t>(std::max(0, std::min(255, tmp)));
    }
  }
}

void QMat::DeQuantiseToRealMat(CuMatrixBase<BaseFloat>* M) const {
  // assume M already sized correctly
  int32 stride = M->Stride();
  for(int32 i = 0; i < num_rows_; i++) {
    int32 src_offset = i * num_cols_;
    int32 dst_offset = i * stride;
    for(int32 j = 0; j < num_cols_; j++) {
      M->Data()[dst_offset + j] = scale * float(int32(cData()[src_offset + j]) - int32(zeropoint));
    }
  }
}

void QMat::DeQuantiseAddToRealMat(CuMatrixBase<BaseFloat>* M) const {
  int32 stride = M->Stride();
  // assume M already sized correctly
  for(int32 i = 0; i < num_rows_; i++) {
    int32 src_offset = i * num_cols_;
    int32 dst_offset = i * stride;
    for(int32 j = 0; j < num_cols_; j++) {
      float val = scale * float(int32(cData()[src_offset + j]) - int32(zeropoint));
      M->Data()[dst_offset + j] += val;
    }
  }
}

static void SetQuantParams(double minval, double maxval, QMat* qmat) {
    minval = std::min(minval, 0.);
    maxval = std::max(maxval, 0.);

    double sscale = (maxval - minval) / 255.;
    // initial zero point
    double init_zp = -minval / sscale;
    
    double nudged_zp = std::round(-minval / sscale);
    minval -= (nudged_zp - init_zp) * sscale;
    maxval -= (nudged_zp - init_zp) * sscale;
    qmat->minval_ = minval;
    qmat->maxval_ = maxval;
    qmat->scale = (float) sscale;
    qmat->zeropoint = static_cast<uint8_t>(nudged_zp);
}

}  // namespace kaldi