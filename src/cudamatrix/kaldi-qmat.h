/*
    For storing uint8_t matrices. Calculations are done via QNNPACK (not with this class).
*/
#include "matrix/matrix-common.h"
#include "cudamatrix/cu-matrix.h"
#include "util/kaldi-io.h"
#include <iostream>
#include <string>

namespace kaldi {

class QMat {
  public:
    QMat() { 
      num_cols_ = 0;
      num_rows_ = 0;
      num_elem_ = 0;
      holding_mem = false;
    }

    QMat(int32 num_rows, int32 num_cols, float s,
        uint8_t zp, double minval) { 
      num_cols_ = num_cols;
      num_rows_ = num_rows;
      num_elem_ = num_cols * num_rows;
      holding_mem = false;
      scale = s;
      zeropoint = zp;
      minval_ = minval;
    }

    inline MatrixIndexT NumElem() const { return num_elem_; }
    inline MatrixIndexT NumRows() const { return num_rows_; }
    inline MatrixIndexT NumCols() const { return num_cols_; }

    inline uint8_t*& Data() { return data_; }

    inline const uint8_t* cData() const { return data_; }

    inline uint8_t& operator() (MatrixIndexT r, MatrixIndexT c) {
        return *(data_ + r*num_cols_ + c);
    }
    
    QMat& operator= (const QMat& other) {
      Init(other.num_rows_, other.num_cols_, other.scale, other.zeropoint, other.minval_);
      for(int32 i = 0; i < num_elem_; i++) {
        this->Data()[i] = other.cData()[i];
      }
      return *this;
    }

    void Copy(const QMat& other) {
      Init(other.num_rows_, other.num_cols_, other.scale, other.zeropoint, other.minval_);
      for(int32 i = 0; i < num_elem_; i++) {
        this->Data()[i] = other.cData()[i];
      }
    }

    void Init(int32 num_rows, int32 num_cols, float s,
        uint8_t zp, double minval);

    void Init(int32 num_rows, int32 num_cols, double minval,
      double maxval);

    void Resize(int32 num_rows, int32 num_cols, bool alloc = true);

    void Read(std::istream & in, bool binary);

    void Write(std::ostream & out, bool binary) const;

    void SetQuantiseFromRealMat(const MatrixBase<BaseFloat>* M);
    
    void SetQuantiseFromRealMat(const CuMatrixBase<BaseFloat>* M);

    void SetQuantiseFromRealMatCentile(const CuMatrixBase<BaseFloat>* M, BaseFloat perc);

    void QuantiseRealMat(const CuMatrixBase<BaseFloat>* M);

    void DeQuantiseToRealMat(CuMatrixBase<BaseFloat>* M) const;

    void DeQuantiseAddToRealMat(CuMatrixBase<BaseFloat>* M) const;

    ~QMat() {
      if (holding_mem) {
        delete[] data_;
        data_ = NULL;
      }
    }

    float scale, minval_, maxval_;
    uint8_t zeropoint;

  protected:

    uint8_t* data_;
    MatrixIndexT num_elem_;
    MatrixIndexT num_rows_;
    MatrixIndexT num_cols_;
    bool holding_mem;
};

static void SetQuantParams(double minval, double maxval, QMat* qmat);

}