// featbin/compute-fbank-feats.cc

// Copyright 2009-2012  Microsoft Corporation
//                      Johns Hopkins University (author: Daniel Povey)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "feat/feature-mfcc.h"
#include "feat/wave-reader.h"


int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    const char *usage =
        "Create MFCC feature files.\n"
        "Usage:  compute-fbank-feats [options...] <wav-rspecifier> <segments-file> <feats-wspecifier>\n";

    // construct all the global objects
    ParseOptions po(usage);
    MfccOptions mfcc_opts;
    bool subtract_mean = false;
    BaseFloat vtln_warp = 1.0;
    std::string vtln_map_rspecifier;
    std::string utt2spk_rspecifier;
    int32 channel = 0;
    BaseFloat min_duration = 0.0;
    bool do_specaugment = false;
    BaseFloat max_overshoot = 0.5;
    // Define defaults for gobal options
    std::string output_format = "kaldi";
    std::string num_frames_wspecifier;
    // Register the option struct
    mfcc_opts.Register(&po);
    // Register the options
    po.Register("output-format", &output_format, "Format of the output files [kaldi, htk]");
    po.Register("subtract-mean", &subtract_mean, "Subtract mean of each feature file [CMS]; not recommended to do it this way. ");
    po.Register("vtln-warp", &vtln_warp, "Vtln warp factor (only applicable if vtln-map not specified)");
    po.Register("vtln-map", &vtln_map_rspecifier, "Map from utterance or speaker-id to vtln warp factor (rspecifier)");
    po.Register("utt2spk", &utt2spk_rspecifier, "Utterance to speaker-id map (if doing VTLN and you have warps per speaker)");
    po.Register("channel", &channel, "Channel to extract (-1 -> expect mono, 0 -> left, 1 -> right)");
    po.Register("min-duration", &min_duration, "Minimum duration of segments to process (in seconds).");
    po.Register("specaugment", &do_specaugment, "Do specaugment (time and frequency masking).");
    po.Register("write-num-frames", &num_frames_wspecifier,
                "Wspecifier to write length in frames of each utterance. "
                "e.g. 'ark,t:utt2num_frames'.  Only applicable if writing tables, "
                "not when this program is writing individual files.  See also "
                "feat-to-len.");

    // OPTION PARSING ..........................................................
    //

    // parse options (+filling the registered variables)
    po.Read(argc, argv);

    if (po.NumArgs() != 3) {
      po.PrintUsage();
      exit(1);
    }

    std::string wav_rspecifier = po.GetArg(1);
    std::string segments_rxfilename = po.GetArg(2);
    std::string output_wspecifier = po.GetArg(3);
    KALDI_LOG << "HHIIIII";
    KALDI_LOG << wav_rspecifier << " " << segments_rxfilename << " " <<output_wspecifier;
    Mfcc mfcc(mfcc_opts);

    int32 compression_method_in = 1;
    CompressionMethod compression_method = static_cast<CompressionMethod>(
        compression_method_in);

    RandomAccessTableReader<WaveHolder> reader(wav_rspecifier);
    Input ki(segments_rxfilename);
    CompressedMatrixWriter kaldi_writer;  // typedef to TableWriter<something>.

    if (utt2spk_rspecifier != "")
      KALDI_ASSERT(vtln_map_rspecifier != "" && "the utt2spk option is only "
                   "needed if the vtln-map option is used.");
    RandomAccessBaseFloatReaderMapped vtln_map_reader(vtln_map_rspecifier,
                                                      utt2spk_rspecifier);

    if (output_format == "kaldi") {
      if (!kaldi_writer.Open(output_wspecifier))
        KALDI_ERR << "Could not initialize output with wspecifier "
                  << output_wspecifier;
    } else {
      KALDI_ERR << "Invalid output_format string " << output_format;
    }
    Int32Writer num_frames_writer(num_frames_wspecifier);

    int32 num_utts = 0, num_success = 0;
    std::string line;
    /* read each line from segments file */
    while (std::getline(ki.Stream(), line)) {
      num_utts++;
      std::vector<std::string> split_line;

      SplitStringToVector(line, " \t\r", true, &split_line);
      if (split_line.size() != 4 && split_line.size() != 5) {
        KALDI_WARN << "Invalid line in segments file: " << line;
        continue;
      }
      std::string segment = split_line[0],
          recording = split_line[1],
          start_str = split_line[2],
          end_str = split_line[3];

      double start, end;
      if (!ConvertStringToReal(start_str, &start)) {
        KALDI_WARN << "Invalid line in segments file [bad start]: " << line;
        continue;
      }
      if (!ConvertStringToReal(end_str, &end)) {
        KALDI_WARN << "Invalid line in segments file [bad end]: " << line;
        continue;
      }
      // start time must not be negative; start time must not be greater than
      // end time, except if end time is -1
      if (start < 0 || (end != -1.0 && end <= 0) || ((start >= end) && (end > 0))) {
        KALDI_WARN << "Invalid line in segments file [empty or invalid segment]: "
                   << line;
        continue;
      }

      if (!reader.HasKey(recording)) {
        KALDI_WARN << "Could not find recording " << recording
                   << ", skipping segment " << segment;
        continue;
      }

      const WaveData &wave = reader.Value(recording);
      const Matrix<BaseFloat> &wave_data = wave.Data();
      BaseFloat samp_freq = wave.SampFreq();  // read sampling fequency
      int32 num_samp = wave_data.NumCols(),  // number of samples in recording
        num_chan = wave_data.NumRows();  // number of channels in recording

      int32 start_samp = start * samp_freq,
          end_samp = (end != -1)? (end * samp_freq) : num_samp;
      if (start_samp < 0 || start_samp > end_samp) {
        KALDI_WARN << segment << " " << start_samp << " " << end_samp << " " << num_samp;
        KALDI_ERR << "Invalid start or end.";
      }
      if (end_samp >= num_samp) {
        if (end_samp < num_samp + max_overshoot * (float) samp_freq) {
          end_samp = num_samp - 1;
        } else {
          KALDI_WARN << "End sample to large " << end_samp << " " << num_samp;
          continue;
        }
      }
      if (num_chan != 1) {
        KALDI_ERR << "More than one channel " << recording;
      }

      SubMatrix<BaseFloat> segment_matrix(wave_data, channel, 1, start_samp, end_samp-start_samp);
      SubVector<BaseFloat> waveform(segment_matrix, 0);
      Matrix<BaseFloat> features;

      try {
        mfcc.ComputeFeatures(waveform, samp_freq, vtln_warp, &features, do_specaugment);
      } catch (...) {
        KALDI_WARN << "Failed to compute features for utterance "
                   << segment;
        continue;
      }
      if (subtract_mean) {
        Vector<BaseFloat> mean(features.NumCols());
        mean.AddRowSumMat(1.0, features);
        mean.Scale(1.0 / features.NumRows());
        for (int32 i = 0; i < features.NumRows(); i++)
          features.Row(i).AddVec(-1.0, mean);
      }

      kaldi_writer.Write(segment, CompressedMatrix(features, compression_method));
      num_frames_writer.Write(segment,
                              features.NumRows());

      if (num_utts % 10 == 0)
        KALDI_LOG << "Processed " << num_utts << " utterances";
      KALDI_VLOG(2) << "Processed features for key " << segment;
      num_success++;
    }
    KALDI_LOG << " Done " << num_success << " out of " << num_utts
              << " utterances.";
    return (num_success != 0 ? 0 : 1);
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
  return 0;
}

