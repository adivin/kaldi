// src/rnnlm/rnnlm-compute-state.h

// Copyright 2017 Johns Hopkins University (author: Daniel Povey)
//           2017 Yiming Wang
//           2017 Hainan Xu

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#ifndef KALDI_RNNLM_COMPUTE_STATE_H_
#define KALDI_RNNLM_COMPUTE_STATE_H_

#include <vector>
#include "base/kaldi-common.h"
#include "nnet3/nnet-optimize.h"
#include "nnet3/nnet-compute.h"
#include "nnet3/am-nnet-simple.h"
#include "rnnlm/rnnlm-core-compute.h"
#include "util/simple-io-funcs.h"

namespace kaldi {
namespace rnnlm {

struct RnnlmComputeStateComputationOptions {
  bool debug_computation;
  bool normalize_probs;
  // We need this when we initialize the RnnlmComputeState and pass the BOS history.
  int32 bos_index;
  // We need this to compute the Final() cost of a state.
  int32 eos_index;
  // This is not needed for computation; included only for ease of scripting.
  int32 brk_index;
  int32 unk_index;
  int32 vocab_size;
  int32 rnnlm_vocab_size;
  int32 ini_index;
  int32 num_index;
  std::vector<int32> id_mapping;

  std::vector<std::string> files;

  nnet3::NnetOptimizeOptions optimize_config;
  nnet3::NnetComputeOptions compute_config;
  RnnlmComputeStateComputationOptions():
      debug_computation(false),
      normalize_probs(false),
      bos_index(-1),
      eos_index(-1),
      brk_index(-1)
      { }

  RnnlmComputeStateComputationOptions(int32 idx_bos, int32 idx_eos, int32 idx_brk, int32 idx_unk,
          int32 idx_ini, int32 sz_vocab, int32 rnnlm_vocab_sz, std::string infodir): debug_computation(false), normalize_probs(false) {
      bos_index = idx_bos;
      eos_index = idx_eos;
      brk_index = idx_brk;
      unk_index = idx_unk;
      ini_index = idx_ini;
      vocab_size = sz_vocab;
      rnnlm_vocab_size = rnnlm_vocab_sz;
      std::string fini = infodir + "/ini.int";
      std::vector<int32> ini_ids;
      readNumsFromFile(fini, ini_ids);

      std::string fin = infodir + "/id_mapping.bin";
      FILE* f = fopen(fin.c_str(), "rb");
      if (!f) KALDI_ERR << "Could not open file " << fin;

      char flen[4];
      fread(flen, 1, 4, f);
      int32 len = *(int*) &flen[0];
      char* text = new char[len];
      fread(text, 1, len, f);
      fclose(f);

      char* s = text;
      std::vector<int32> ngram_ids;
      std::vector<int32> rnn_ids;
      ngram_ids.reserve(rnnlm_vocab_size);
      rnn_ids.reserve(rnnlm_vocab_size);
      while (true) {
        int32 offset = (int32) (s - &text[0]);
        if (offset >= len) break;
        int32 n = parseInt(s);
        ngram_ids.push_back(n);
        s++;
        n = parseInt(s);
        rnn_ids.push_back(n);
        s++;
      }

      int32 sz = ngram_ids.size();
      id_mapping.resize(sz_vocab, idx_unk);
      for(int32 i=0; i < sz; i++) {
          int32 ngram_idx = ngram_ids[i];
          int32 rnn_idx = rnn_ids[i];
          id_mapping[ngram_idx] = rnn_idx;
      }
      sz = ini_ids.size();
      for(int32 i = 0; i < sz; i++) {
          int32 ngram_idx = ini_ids[i];
          id_mapping[ngram_idx] = ini_index;
      }
  }

  void Register(OptionsItf *opts) {
    opts->Register("debug-computation", &debug_computation, "If true, turn on "
                   "debug for the actual computation (very verbose!)");
    opts->Register("normalize-probs", &normalize_probs, "If true, word "
       "probabilities will be correctly normalized (otherwise the sum-to-one "
       "normalization is approximate)");
    opts->Register("bos-symbol", &bos_index, "Index in wordlist representing "
                   "the begin-of-sentence symbol");
    opts->Register("eos-symbol", &eos_index, "Index in wordlist representing "
                   "the end-of-sentence symbol");
    opts->Register("brk-symbol", &brk_index, "Index in wordlist representing "
                   "the break symbol. It is not needed in the computation "
                   "and we are including it for ease of scripting");

    // Register the optimization options with the prefix "optimization".
    ParseOptions optimization_opts("optimization", opts);
    optimize_config.Register(&optimization_opts);

    // Register the compute options with the prefix "computation".
    ParseOptions compute_opts("computation", opts);
    compute_config.Register(&compute_opts);
  }
};

/*
  This class const references to the word-embedding, nnet3 part of rnnlm and
the RnnlmComputeStateComputationOptions. It handles the computation of the nnet3
object
*/
class RnnlmComputeStateInfo  {
 public:
  RnnlmComputeStateInfo(
      const RnnlmComputeStateComputationOptions &opts,
      const kaldi::nnet3::Nnet &rnnlm,
      const CuMatrix<BaseFloat> &word_embedding_mat);

  const RnnlmComputeStateComputationOptions &opts;
  const kaldi::nnet3::Nnet &rnnlm;
  const CuMatrix<BaseFloat> &word_embedding_mat;

  // The compiled, 'looped' computation.
  nnet3::NnetComputation computation;
};

/*
  This class handles the neural net computation; it's mostly accessed
  via other wrapper classes.

  Each time this class takes a new word and advance the NNET computation by
  one step, and works out log-prob of words to be used in lattice rescoring. */

class RnnlmComputeState {
 public:
  /// We compile the computation and generate the state after the BOS history.
  RnnlmComputeState(const RnnlmComputeStateInfo &info, int32 bos_index);

  RnnlmComputeState(const RnnlmComputeState &other);

  /// Generate another state by passing the next-word.
  /// The pointer is owned by the caller.
  RnnlmComputeState* GetSuccessorState(int32 next_word) const;

  /// Return the log-prob that the model predicts for the provided word-index,
  /// given the previous history determined by the sequence of calls to AddWord()
  /// (implicitly starting with the BOS symbol).
  BaseFloat LogProbOfWord(int32 word_index) const;

  // This function computes logprobs of all words and set it to output Matrix
  // Note: (*output)(0, 0) corresponds to <eps> symbol and it should NEVER be
  // used in any computation by the caller. To avoid causing unexpected issues,
  // we here set it to a very small number
  void GetLogProbOfWords(CuMatrixBase<BaseFloat>* output) const;
  /// Advance the state of the RNNLM by appending this word to the word sequence.
  void AddWord(int32 word_index);
 private:
  /// This function does the computation for the next chunk.
  void AdvanceChunk();

  const RnnlmComputeStateInfo &info_;
  nnet3::NnetComputer computer_;
  int32 previous_word_;

  // This is the log of the sum of the exp'ed values in the output.
  // Only used if config_.normalize_probs is set to be true.
  BaseFloat normalization_factor_;

  // This points to the matrix returned by GetOutput() on the Nnet object.
  // This pointer is not owned by this class.
  const CuMatrixBase<BaseFloat> *predicted_word_embedding_;
};

class RnnlmComputeStateInfoAdapt  {
 public:
  RnnlmComputeStateInfoAdapt(
      const RnnlmComputeStateComputationOptions &opts,
      const kaldi::nnet3::Nnet &rnnlm,
      const CuMatrix<BaseFloat> &word_embedding_mat_large,
      const CuMatrix<BaseFloat> &word_embedding_mat_med,
      const CuMatrix<BaseFloat> &word_embedding_mat_small,
      const int32 cutofflarge,
      const int32 cutoffmed);

  const RnnlmComputeStateComputationOptions &opts;
  const kaldi::nnet3::Nnet &rnnlm;
  const CuMatrix<BaseFloat> &word_embedding_mat_large;
  const CuMatrix<BaseFloat> &word_embedding_mat_med;
  const CuMatrix<BaseFloat> &word_embedding_mat_small;
  const int32 cutoff_large;
  const int32 cutoff_med;

  int32 total_emb_size_;
  // The compiled, 'looped' computation.
  nnet3::NnetComputation computation;
};

/*
  This class handles the neural net computation; it's mostly accessed
  via other wrapper classes.

  Each time this class takes a new word and advance the NNET computation by
  one step, and works out log-prob of words to be used in lattice rescoring. */

class RnnlmComputeStateAdapt {
 public:
  /// We compile the computation and generate the state after the BOS history.
  RnnlmComputeStateAdapt(const RnnlmComputeStateInfoAdapt &info, int32 bos_index);

  RnnlmComputeStateAdapt(const RnnlmComputeStateAdapt &other);

  /// Generate another state by passing the next-word.
  /// The pointer is owned by the caller.
  RnnlmComputeStateAdapt* GetSuccessorState(int32 next_word) const;

  /// Return the log-prob that the model predicts for the provided word-index,
  /// given the previous history determined by the sequence of calls to AddWord()
  /// (implicitly starting with the BOS symbol).
  BaseFloat LogProbOfWord(int32 word_index) const;


  /// Advance the state of the RNNLM by appending this word to the word sequence.
  void AddWord(int32 word_index);

 private:
  /// This function does the computation for the next chunk.
  void AdvanceChunk();

  const RnnlmComputeStateInfoAdapt &info_;
  nnet3::NnetComputer computer_;
  int32 previous_word_;

  // This is the log of the sum of the exp'ed values in the output.
  // Only used if config_.normalize_probs is set to be true.
  BaseFloat normalization_factor_;

  // This points to the matrix returned by GetOutput() on the Nnet object.
  // This pointer is not owned by this class.
  const CuMatrixBase<BaseFloat> *predicted_word_embedding_large_;
  const CuMatrixBase<BaseFloat> *predicted_word_embedding_med_;
  const CuMatrixBase<BaseFloat> *predicted_word_embedding_small_;
};

} // namespace rnnlm
} // namespace kaldi

#endif  // KALDI_RNNLM_COMPUTE_STATE_H_
