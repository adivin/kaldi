// rnnlm/rnnlm-utils.cc

// Copyright 2017  Daniel Povey
//           2017  Hossein Hadian

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include <numeric>
#include "rnnlm/rnnlm-utils.h"

namespace kaldi {
namespace rnnlm {


void ReadSparseWordFeatures(std::istream &is,
                            int32 feature_dim,
                            SparseMatrix<BaseFloat> *word_feature_matrix) {
  std::vector<std::vector<std::pair<MatrixIndexT, BaseFloat> > > sparse_rows;
  std::string line;
  int32 line_number = 0;
  while (std::getline(is, line)) {
    std::vector<std::pair<MatrixIndexT, BaseFloat> > row;
    std::istringstream line_is(line);
    int32 word_id;
    line_is >> word_id;
    line_is >> std::ws;
    if (word_id != line_number++)
      KALDI_ERR << "The word-indexes are expected to be in order 0, 1, 2, ...";

    int32 feature_index;
    BaseFloat feature_value;
    while (line_is >> feature_index)
    {
      if (!(feature_index >= 0 && feature_index < feature_dim))
        KALDI_ERR << "Invalid feature index: " << feature_index
                  << ". Feature indexes should be in the range [0, feature_dim)"
                  << " where feature_dim is " << feature_dim;
      line_is >> std::ws;
      if (!(line_is >> feature_value))
        KALDI_ERR << "No value for feature-index " << feature_index;
      row.push_back(std::make_pair(feature_index, feature_value));
      if (row.size() > 1 && row.back().first <= row.rbegin()[1].first)
        KALDI_ERR << "feature indexes are expected to be in increasing order."
                  << " Faulty line: " << line;
    }
    sparse_rows.push_back(row);
  }
  if (sparse_rows.size() < 1)
    KALDI_ERR << "No line could be read from the file.";
  word_feature_matrix->CopyFromSmat(
      SparseMatrix<BaseFloat>(feature_dim, sparse_rows));
}

int readInt(const char*& s) {
  int32 n = *s - '0';
  int32 d;
  while ((d = *++s - '0') >= 0) {
    n = n * 10 + d;
  }
  return n;
}

BaseFloat readFloat(const char*& s) {
  int32 sign = 1;
  if (*s == '-') {
    sign = -1;
    s++;
  }
  BaseFloat d;
  BaseFloat acc = *s - '0';
  while((d = *++s - '0') >= 0) {
    acc = acc * 10.0 + d;
  }

  if (*s == '.') {
    BaseFloat k = 0.1;
    while((d = *++s - '0') >= 0 && d <= 9) {
      acc += k * d;
      k *= 0.1;
    }
  }

  if (*s == 'e') {
    s += 2;  // skipping e-
    BaseFloat e = *s - '0';
    while((d = *++s - '0') >= 0) {
      e = 10.0 * e + d;
    }
    for(int16 i = 0; i < e; i++) {
      acc /= 10.0;
    }
  }

  return sign * acc;
}

void ReadSparseWordFeaturesBinary(std::string fin,
                                  int32 feature_dim,
                                  SparseMatrix<BaseFloat> *word_feature_matrix) {
  std::vector<std::vector<std::pair<MatrixIndexT, BaseFloat> > > sparse_rows;
  FILE* f = fopen(fin.c_str(), "rb");
  if (!f) KALDI_ERR << "Word feat binary file does not exist";

  char flen[4];
  for(int16_t i=0; i < 4;i++) flen[i] = fgetc(f);
  int32 len = *(int*) &flen[0];
  char* text = new char[len];
  fread(text, 1, len, f);
  fclose(f);

  const char* s = text;
  while(true) {
    int32 offset = (int) (s - &text[0]);
    if (text[offset] == '\0' || (offset + 4) >= len) break;

    std::vector<std::pair<MatrixIndexT, BaseFloat> > row;

    int32 word_id = readInt(s);
    int32 feat_index;
    BaseFloat feat_value;
    int32 d = *s - '0';

    if (d == -16) d = 0;
    while(d >= 0) {
      s++;  // going over space
      feat_index = readInt(s);
      s++;  // going over space
      feat_value = readFloat(s);
      row.push_back(std::make_pair(feat_index, feat_value));
      d = *s - '0';
      if (d == -16) {
        d = 0;
      }
    }
    sparse_rows.push_back(row);
    s++;
  }
  delete[] text;

  word_feature_matrix->CopyFromSmat(
      SparseMatrix<BaseFloat>(feature_dim, sparse_rows));
}


}  // namespace rnnlm
}  // namespace kaldi
