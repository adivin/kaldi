#!/bin/bash

. ./cmd.sh
. ./path.sh


# You may set 'mic' to:
#  ihm [individual headset mic- the default which gives best results]
#  sdm1 [single distant microphone- the current script allows you only to select
#        the 1st of 8 microphones]
#  mdm8 [multiple distant microphones-- currently we only support averaging over
#       the 8 source microphones].
# ... by calling this script as, for example,
# ./run.sh --mic sdm1
# ./run.sh --mic mdm8
mic=

# Train systems,
nj=10 # number of parallel jobs,
stage=0
. utils/parse_options.sh

base_mic=$(echo $mic | sed 's/[0-9]//g') # sdm, ihm or mdm
nmics=$(echo $mic | sed 's/[a-z]//g') # e.g. 8 for mdm8.

set -euo pipefail

# Path where AMI gets downloaded (or where locally available):
AMI_DIR=/data/am-data/ami # Default,

LM=o3_pr7

PROCESSED_AMI_DIR=$AMI_DIR

if [ $stage -le 0 ]; then
    local/ami_text_prep.sh data/local/downloads
    local/ami_ihm_data_prep.sh $PROCESSED_AMI_DIR ihm
    local/ami_ihm_scoring_data_prep.sh $PROCESSED_AMI_DIR ihm dev
    local/ami_ihm_scoring_data_prep.sh $PROCESSED_AMI_DIR ihm eval
fi

if [ $stage -le 1 ]; then
    seconds_per_spk_max=120
    #for dset in train dev eval; do
    #    utils/data/modify_speaker_info.sh --seconds-per-spk-max $seconds_per_spk_max \
    #        data/${dset}_orig data/$dset
    #done
fi

# Preparing text, creating lexicon and LM
if [ $stage -le 2 ]; then
    local/train_prep_ami.sh $PWD
fi
exit 0
if [ $stage -le 3 ]; then
    local/ami_prep_lang.sh
fi

# Feature extraction,
if [ $stage -le 4 ]; then
  for dset in train dev eval; do
    steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" data/$dset
    steps/compute_cmvn_stats.sh data/$dset
    utils/fix_data_dir.sh data/$dset
  done
fi

# monophone training
if [ $stage -le 5 ]; then
  # Full set 77h, reduced set 10.8h,
  utils/subset_data_dir.sh data/train 15000 data/train_15k

  steps/train_mono.sh --nj $nj --cmd "$train_cmd" \
    data/train_15k data/lang exp/mono
  steps/align_si.sh --nj $nj --cmd "$train_cmd" \
    data/train data/lang exp/mono exp/mono_ali
fi

# context-dep. training with delta features.
if [ $stage -le 6 ]; then
  steps/train_deltas.sh --cmd "$train_cmd" \
    5000 30000 data/train data/lang exp/mono_ali exp/tri1
  steps/align_si.sh --nj $nj --cmd "$train_cmd" \
    data/train data/lang exp/tri1 exp/tri1_ali
fi

if [ $stage -le 7 ]; then
  # LDA_MLLT
  steps/train_lda_mllt.sh --cmd "$train_cmd" \
    --splice-opts "--left-context=3 --right-context=3" \
    5000 30000 data/train data/lang exp/tri1_ali exp/tri2
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    data/train data/lang exp/tri2 exp/tri2_ali
#  # Decode
  # graph_dir=exp/tri2/graph_${LM}
  #$decode_cmd --mem 4G $graph_dir/mkgraph.log \
  #  utils/mkgraph.sh data/lang_${LM} exp/tri2 $graph_dir
  #steps/decode.sh --nj 11 --cmd "$decode_cmd" --config conf/decode.conf \
  #  $graph_dir data/dev exp/tri2/decode_dev_${LM}
  #steps/decode.sh --nj 11 --cmd "$decode_cmd" --config conf/decode.conf \
  #  $graph_dir data/eval exp/tri2/decode_eval_${LM}
fi


if [ $stage -le 8 ]; then
  # LDA+MLLT+SAT
  steps/train_sat.sh --cmd "$train_cmd" \
    5000 30000 data/train data/lang exp/tri2_ali exp/tri3
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    data/train data/lang exp/tri3 exp/tri3_ali
fi

if [ $stage -le 9 ]; then
  # Decode the fMLLR system.
  graph_dir=exp/tri3/graph_${LM}
  #$decode_cmd --mem 4G $graph_dir/mkgraph.log \
  #  utils/mkgraph.sh data/lang_${LM} exp/tri3 $graph_dir
  #steps/decode_fmllr.sh --nj 11 --cmd "$decode_cmd" --config conf/decode.conf \
  #  $graph_dir data/dev exp/tri3/decode_dev_${LM}
  #steps/decode_fmllr.sh --nj 11 --cmd "$decode_cmd" --config conf/decode.conf \
  #  $graph_dir data/eval exp/tri3/decode_eval_${LM}
fi

if [ $stage -le 10 ]; then
  # The following script cleans the data and produces cleaned data
  # in data/train_cleaned, and a corresponding system
  # in exp/tri3_cleaned.  It also decodes.
  #
  # Note: local/run_cleanup_segmentation.sh defaults to using 50 jobs,
  # you can reduce it using the --nj option if you want.
  local/run_cleanup_segmentation.sh --nj 10
fi

if [ $stage -le 11 ]; then
  ali_opt=
  local/chain/run_tdnn.sh
fi

#if [ $stage -le 12 ]; then
#  the following shows how you would run the nnet3 system; we comment it out
#  because it's not as good as the chain system.
#  ali_opt=
#  [ "$mic" != "ihm" ] && ali_opt="--use-ihm-ali true"
# local/nnet3/run_tdnn.sh $ali_opt --mic $mic
#fi

exit 0
