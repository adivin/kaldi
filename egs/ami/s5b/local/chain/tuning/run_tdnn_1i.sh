#!/bin/bash

# same as 1h but replacing proportional-shrink with l2-regularize.
# The results match those from 1h.

# local/chain/tuning/run_tdnn_1i.sh --mic sdm1 --use-ihm-ali true --train-set train_cleaned  --gmm tri3_cleaned

# local/chain/compare_wer_general.sh sdm1 tdnn1h_sp_bi_ihmali tdnn1i_sp_bi_ihmali
# System                tdnn1h_sp_bi_ihmali tdnn1i_sp_bi_ihmali
# WER on dev        36.6      36.6
# WER on eval        40.5      40.6
# Final train prob      -0.193585 -0.196231
# Final valid prob      -0.265758 -0.265572
# Final train prob (xent)      -2.32497  -2.48061
# Final valid prob (xent)      -2.60964  -2.71794


set -e -o pipefail
# First the options that are passed through to run_ivector_common.sh
# (some of which are also used in this script directly).
stage=12
mic=
nj=10
min_seg_len=1.55
train_set=train_cleaned
gmm=tri3_cleaned  # the gmm for the target data
ihm_gmm=tri3  # the gmm for the IHM system (if --use-ihm-ali true).
num_threads_ubm=4
ivector_transform_type=pca
nnet3_affix=_cleaned  # cleanup affix for nnet3 and chain dirs, e.g. _cleaned
remove_egs=true

# The rest are configs specific to this script.  Most of the parameters
# are just hardcoded at this level, in the commands below.
train_stage=-10
tree_affix=nosp_beam13_largelrdecay  # affix for tree directory, e.g. "a" or "b", in case we change the configuration.
tdnn_affix=_fbank_delta_beam13_largelr  #affix for TDNN directory, e.g. "a" or "b", in case we change the configuration.
common_egs_dir=  # you can set this to use previously dumped egs.


# End configuration section.
echo "$0 $@"  # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh


if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

gmm_dir=exp/$gmm
lores_train_data_dir=data/${train_set}_comb
tree_dir=exp/chain${nnet3_affix}/tree_bi${tree_affix}
#ali_dir=exp/${gmm}_ali_${train_set}_comb
#lat_dir=exp/chain${nnet3_affix}/${gmm}_${train_set}_comb_lats
ali_dir=exp/tri3_ali_cleaned
lat_dir=exp/tri3_cleaned_lat
dir=exp/chain${nnet3_affix}/tdnn${tdnn_affix}

train_data_dir=data/${train_set}_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_comb
LM=o3_pr7


#if [ $stage -le 8 ]; then
#    utils/data/perturb_data_dir_speed_3way.sh data/${train_set} data/${train_set}_sp
#fi

if [ $stage -le 9 ]; then
    #utils/data/perturb_data_dir_volume.sh data/${train_set}_sp
    steps/make_mfcc.sh --nj $nj --mfcc-config conf/mfcc.conf --cmd "$train_cmd" data/${train_set}
fi

if [ $stage -le 10 ]; then
    utils/data/combine_short_segments.sh data/${train_set} $min_seg_len data/${train_set}_comb
    steps/compute_cmvn_stats.sh data/${train_set}_comb
fi

if [ $stage -le 11 ]; then
  #if [ -f $ali_dir/ali.1.gz ]; then
  #  echo "$0: alignments in $ali_dir appear to already exist.  Please either remove them "
  #  echo " ... or use a later --stage option."
  #  exit 1
  #fi
  echo "$0: aligning perturbed, short-segment-combined ${maybe_ihm}data"
  #steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
  #   data/${train_set}_comb data/lang $gmm_dir $ali_dir
fi

#[ ! -f $ali_dir/ali.1.gz ] && echo  "$0: expected $ali_dir/ali.1.gz to exist" && exit 1


if [ $stage -le 12 ]; then
  # Get the alignments as lattices (gives the chain training more freedom).
  # use the same num-jobs as the alignments
  steps/align_fmllr_lats.sh --nj 10 --cmd "$train_cmd" data/${train_set}_comb \
    data/lang $gmm_dir $lat_dir
  rm $lat_dir/fsts.*.gz # save space
fi

if [ $stage -le 13 ]; then
  echo "$0: creating lang directory with one state per phone."
  # Create a version of the lang/ directory that has one state per phone in the
  # topo file. [note, it really has two states.. the first one is only repeated
  # once, the second one has zero or more repeats.]
  if [ -d data/lang_chain ]; then
    if [ data/lang_chain/L.fst -nt data/lang/L.fst ]; then
      echo "$0: data/lang_chain already exists, not overwriting it; continuing"
    else
      echo "$0: data/lang_chain already exists and seems to be older than data/lang..."
      echo " ... not sure what to do.  Exiting."
      exit 1;
    fi
  else
    cp -r data/lang data/lang_chain
    silphonelist=$(cat data/lang_chain/phones/silence.csl) || exit 1;
    nonsilphonelist=$(cat data/lang_chain/phones/nonsilence.csl) || exit 1;
    # Use our special topology... note that later on may have to tune this
    # topology.
    steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >data/lang_chain/topo
  fi
fi


if [ $stage -le 14 ]; then
  # Build a tree using our new topology.  We know we have alignments for the
  # speed-perturbed data (local/nnet3/run_ivector_common.sh made them), so use
  # those.
  if [ -f $tree_dir/final.mdl ]; then
    echo "$0: $tree_dir/final.mdl already exists, refusing to overwrite it."
    exit 1;
  fi
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
      --context-opts "--context-width=2 --central-position=1" \
      --leftmost-questions-truncate -1 \
      --cmd "$train_cmd" 4200 data/${train_set}_comb data/lang_chain $lat_dir $tree_dir
      # lat_dir
fi

train_data_dir=data/train_fbank_comb
if [ $stage -le 15 ]; then
    #cp -r data/${train_set} data/train_fbank
    #rm -rf data/train_fbank/{data,feats.scp,cmvn.scp}
    ##ut_tils/data/perturb_data_dir_speed_3way.sh data/train_fbank data/train_fbank_sp
    #utils/data/perturb_data_dir_volume.sh data/train_fbank
    #steps/make_fbank.sh --nj $nj --fbank-config conf/fbank.conf --cmd "$train_cmd" \
    #    data/train_fbank

    #utils/data/combine_short_segments.sh data/train_fbank $min_seg_len $train_data_dir
    #steps/compute_cmvn_stats.sh $train_data_dir
    echo "make fbank"
fi

xent_regularize=0.1

if [ $stage -le 16 ]; then
  echo "$0: creating neural net configs using the xconfig parser";

  num_targets=$(tree-info $tree_dir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print (0.5/$xent_regularize)" | python)
  opts="l2-regularize=0.02"
  output_opts="l2-regularize=0.004"

  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  input dim=80 name=input

  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor

  # the first splicing is moved before the lda layer, so no splicing here
  relu-batchnorm-layer name=tdnn2 input=Append(-1,0,1) dim=450 $opts
  relu-batchnorm-layer name=tdnn3 dim=450 $opts
  relu-batchnorm-layer name=tdnn4 input=Append(-1,0,1) dim=450 $opts
  relu-batchnorm-layer name=tdnn5 dim=450 $opts
  relu-batchnorm-layer name=tdnn6 input=Append(-3,0,3) dim=450 $opts
  relu-batchnorm-layer name=tdnn7 input=Append(-3,0,3) dim=450 $opts
  relu-batchnorm-layer name=tdnn8 input=Append(-3,0,3) dim=450 $opts

  ## adding the layers for chain branch
  relu-batchnorm-layer name=prefinal-chain input=tdnn8 dim=450 target-rms=0.5 $opts
  output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5 $output_opts

  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  relu-batchnorm-layer name=prefinal-xent input=tdnn8 dim=450 target-rms=0.5 $opts
  output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5 $output_opts

EOF

  steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/
fi

if [ $stage -le 17 ]; then
  if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl \
     /export/b0{5,6,7,8}/$USER/kaldi-data/egs/ami-$(date +'%m_%d_%H_%M')/s5b/$dir/egs/storage $dir/egs/storage
  fi
# --feat.online-ivector-dir $train_ivector_dir \
  steps/nnet3/chain/train.py --stage  $train_stage \
    --cmd "$decode_cmd" \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --egs.dir "$common_egs_dir" \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width 150 \
    --trainer.num-chunk-per-minibatch 128 \
    --trainer.frames-per-iter 1500000 \
    --trainer.num-epochs 7 \
    --trainer.optimization.num-jobs-initial 2 \
    --trainer.optimization.num-jobs-final 2 \
    --trainer.optimization.initial-effective-lrate 0.002 \
    --trainer.optimization.final-effective-lrate 0.0002 \
    --trainer.max-param-change 2.0 \
    --cleanup.remove-egs $remove_egs \
    --feat-dir $train_data_dir \
    --tree-dir $tree_dir \
    --lat-dir $lat_dir \
    --dir $dir
fi


graph_dir=$dir/graph_${LM}
if [ $stage -le 18 ]; then
    # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
    # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
    # the lang directory.
    utils/mkgraph.sh --self-loop-scale 1.0 data/lang_${LM} $dir $graph_dir

    for set in eval dev; do
        echo "Making test feats"
        #rm -rf data/${set}_fbank
        #cp -r data/$set data/${set}_fbank
        #rm -rf data/${set}_fbank/{data,feats.scp,cmvn.scp}

        #steps/make_fbank.sh --nj $nj --fbank-config conf/fbank.conf --cmd "$train_cmd" \
        #    data/${set}_fbank
        #steps/compute_cmvn_stats.sh data/${set}_fbank
    done
fi

if [ $stage -le 19 ]; then
  rm $dir/.error 2>/dev/null || true
  for decode_set in dev eval; do

      steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj 8 --cmd "$decode_cmd" \
          --scoring-opts "--min-lmwt 6 " \
         $graph_dir data/${decode_set}_fbank $dir/decode_${decode_set} || exit 1;

  done
fi
exit 0
