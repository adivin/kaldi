#!/bin/bash -e
indir=$1
    #rm -rf data/
mv $indir/data//text $indir/data//textorig
awk '{for(i=2;i<=NF;i++) {printf $i" "}; printf "\n"}' $indir/data//textorig | perl -pe 's/([A-Z])\.\s(?=[A-Z]\.)/\L\1/g' | perl -pe 's/([A-Z])\.\s(?![A-Z]\.)/\L\1 /' | perl -pe 's/-//g' | sed -r 's/\.//g' > $indir/data/tmptext
python3 local/prep_text.py $indir/data/tmptext | sed -r 's/\bi\b/I/g' > $indir/data//textonly
paste  <( awk '{print $1}' $indir/data//textorig) $indir/data//textonly> $indir/data/$set/text
rm $indir/data//{textorig,textonly}
cp $indir/data//text $indir/data//textlm

rm $indir/data/tmptext
