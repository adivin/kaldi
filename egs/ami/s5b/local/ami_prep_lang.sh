#!/bin/bash -ex
dir=data

rm -rf $dir/lang*
/home/seni/git/condutor/scripts/create_Lfst.sh data/train/textonly $dir/lang
echo "Done prons"

work=$(mktemp -d --tmpdir=/tmp)
echo "Work dir $work"

awk '{print $1}' $dir/lang/words.txt | grep -v '<eps>' | grep -v '#0' | sort -u > $dir/lang/vocab.txt

ngram-count -unk -map-unk "<unk>" -order 3 -limit-vocab -vocab $dir/lang/vocab.txt -kndiscount -interpolate -text data/train/textonly -lm $work/o3.gz

ngram -unk -lm $work/o3.gz -prune 1e-7 -write-lm $work/o3_pr7.gz

utils/format_lm.sh $dir/lang $work/o3_pr7.gz $dir/lang/lex $dir/lang_o3_pr7

rm -rf $work

echo "Done"
exit 0
