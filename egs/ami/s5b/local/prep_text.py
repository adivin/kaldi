#!/usr/bin/env python3
from os.path import join
import regex as re
import sys

p = re.compile('([a-z])\.\s(?=[a-z]\.)')

def main(inf, outf):
    with open(inf) as fh, open(outf, 'w') as fhw:
        text = []
        for line in fh:
            id, *text = line.split()
            nline = []
            for word in text:
                if '.' in word:
                    word = word.lower()
                nline.append(word)
            nline = ' '.join(nline)
            nline = p.sub(r'\1', nline)
            line = []
            for word in nline.split():
                if word.isupper():
                    w = word.lower()
                else:
                    w = word.strip('.').upper()
                line.append(w)
            line = ' '.join(line)
            line = re.sub(r'\bi\b', 'I', line)

            fhw.write(f'{id} {line}\n')

import plac
plac.call(main)
