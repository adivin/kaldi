import plac


class Word():
    def __init__(self, word, time, dur):
        self.word = word
        self.time = time
        self.dur = dur

def main(inctm, inpost, phonestable, wordstable, outf):
    """ Aligns words in ctm with phone posteriors """
    i2w = {}
    with open(wordstable) as fh:
        for line in fh:
            line=line.split()
            i2w[line[1]] = line[0]

    i2p = {}
    with open(phonestable) as fh:
        for line in fh:
            line=line.split()
            i2p[line[1]] = line[0]

    words = []
    with open(inctm) as fh:
        for line in fh:
            line = line.split()
            w = Word(line[4], float(line[2]), float(line[3]))
            words.append(w)
    fhw = open(outf, 'w')
    shift = 0.03
    curtime = 0.  # current time
    wordidx = 0
    with open(inpost) as fh:
        for line in fh:  # should be only 1 line
            while True:
                fhw.write(f'{curtime:.2f}: ')
                startidx = line.find('[')
                endidx = line.find(']')

                if startidx == -1:
                    break

                if wordidx == len(words):
                    fhw.write(f'<eps>\t')
                else:
                    w = words[wordidx]
                    if w.time + w.dur < curtime:
                        wordidx += 1
                if wordidx == len(words):
                    fhw.write(f'<eps>\t')
                else:
                    w = words[wordidx]
                    if w.time < curtime < w.time + w.dur:
                        fhw.write(f'{w.word}\t')
                    else:
                        fhw.write(f'<eps>\t')

                linepart = line[startidx+1:endidx].split()
                assert len(linepart) % 2 == 0
                lst = []
                for i in range(0, len(linepart), 2):
                    phoneidx = linepart[i]
                    prob = linepart[i+1]
                    lst.append((i2p[phoneidx], prob,))
                lst = sorted(lst, key=lambda x: x[1], reverse=True)
                for p, prob in lst:
                    fhw.write(f'{p} ({prob})\t')
                fhw.write('\n')
                curtime += shift
                line = line[endidx+1:]
    fhw.close()


plac.call(main)
