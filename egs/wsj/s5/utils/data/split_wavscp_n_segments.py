import plac
import os
from collections import defaultdict


def main(indata, nj):
    dct_wavs = {}
    with open(f'{indata}/wav.scp') as fh:
        for line in fh:
            wavid, *rest = line.split()
            dct_wavs[wavid] = ' '.join(rest)

    wav2dur = {}
    with open(f'{indata}/reco2dur') as fh:
        for line in fh:
            line = line.split()
            wav2dur[line[0]] = float(line[1])

    wav2seg = defaultdict(list)
    numsegs = 0
    with open(f'{indata}/segments') as fh:
        for line in fh:
            line = line.split()
            wavid = line[1]
            line[1] = ''
            wav2seg[wavid].append(' '.join(line))
            numsegs += 1

    totdur = sum(dur for dur in wav2dur.values())
    print(f'Total audio file duration: {totdur/3600:.2f}h, num segs {numsegs}')
    dur_per_split = totdur / int(nj)

    os.makedirs(f'{indata}/log', exist_ok=True)
    wavids_sorted = sorted(wav2seg.keys())
    idx_wavs = 0
    for i in range(1, int(nj)+1):
        if idx_wavs == len(wavids_sorted):
            break
        targdur = dur_per_split
        wavsused = {}
        durused = 0
        with open(f'{indata}/log/wavscp.{i}', 'w') as fha, open(f'{indata}/log/segments.{i}', 'w') as fhb:
            while durused < targdur:
                wavid = wavids_sorted[idx_wavs]
                line = dct_wavs[wavid]
                durused += wav2dur[wavid]
                fha.write(f'{wavid} {line}\n')
                for line in wav2seg[wavid]:
                    line = line.split()
                    segid = line[0]
                    rest = ' '.join(line[1:])
                    fhb.write(f'{segid} {wavid} {rest}\n')
                idx_wavs += 1
                if idx_wavs == len(wavids_sorted):
                    break

plac.call(main)
