#!/bin/bash -e
set -o pipefail

cmd="queue.pl --mem 1G"
scale_opts="--transition-scale=1.0 --self-loop-scale=0.1"
acoustic_scale=0.1
beam=13.0
lattice_beam=12.0
nj=10
transform_dir=
oov="<unk>"
stage=0

[ -f path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# != 5 ]; then
    echo "Usage: $0 data/train data/lang exp/tri3 /tmp data/train_cleaned"
    exit 1;
fi

data=$1
lang=$2
srcdir=$3
work=$4
dir=$5  # new data
mkdir -p $dir

for f in $data/text $lang/oov.int $srcdir/tree $srcdir/final.mdl \
    $lang/L_disambig.fst $lang/phones/disambig.int; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1;
done

utils/lang/check_phones_compatible.sh $lang/phones.txt $srcdir/phones.txt || exit 1;

#splice_opts=`cat $srcdir/splice_opts 2>/dev/null`
#
#sdata=$data/split$nj
#[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
#
#if [ -f $srcdir/final.mat ]; then feat_type=lda; else feat_type=delta; fi
#echo "$0: feature type is $feat_type"
#
#case $feat_type in
#    delta) feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | add-deltas ark:- ark:- |";;
#    lda) feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- | splice-feats $splice_opts ark:- ark:- | transform-feats $srcdir/final.mat ark:- ark:- |";;
#    *) echo "$0: invalid feature type $feat_type" && exit 1;
#esac
#
#if [ -z "$transform_dir" ] && [ -f $srcdir/trans.1 ]; then
#    transform_dir=$srcdir
#fi
#
#if [ ! -z "$transform_dir" ]; then
#  echo "$0: using transforms from $transform_dir"
#  [ ! -f $transform_dir/trans.1 ] && echo "$0: no such file $transform_dir/trans.1" && exit 1;
#  nj_orig=$(cat $transform_dir/num_jobs)
#  if [ $nj -ne $nj_orig ]; then
#    # Copy the transforms into an archive with an index.
#    for n in $(seq $nj_orig); do cat $transform_dir/trans.$n; done | \
#      copy-feats ark:- ark,scp:$work/trans.ark,$work/trans.scp || exit 1;
#    feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk scp:$work/trans.scp ark:- ark:- |"
#  else
#    # number of jobs matches with alignment dir.
#    feats="$feats transform-feats --utt2spk=ark:$sdata/JOB/utt2spk ark:$transform_dir/trans.JOB ark:- ark:- |"
#  fi
#elif [ -f $srcdir/final.alimdl ]; then
#  echo "$0: **WARNING**: you seem to be using an fMLLR system as input,"
#  echo "  but you are not providing the --transform-dir option during alignment."
#fi


graph=$work/graphs
if [ $stage -le 0 ]; then
	echo "Creating Graphs"
    mkdir -p $graph/fsts
    utils/sym2int.pl --map-oov $oov -f 2- $lang/words.txt $data/text > $work/text.int
    set +e
    awk '{for(i=2;i<=NF;i++) print $i;}' $work/text.int | uniq -c | sort -nr | head -n 100 | awk '{print $2}' > $work/top_words.int
    set -e

    nlines=$(wc -l <$work/text.int)
    nlinesper=$(($nlines/$nj + $nj))
    split -l $nlinesper --numeric-suffixes=1 $work/text.int $work/stext
    rename 's/0*([0-9]+)/$1/' $work/stext*

    $cmd JOB=1:$nj $work/make_graphs.JOB.log \
        /home/seni/git/condutor/scripts/make_unigrams.py $work/stextJOB $work/top_words.int \| \
        compile-train-graphs-fsts $scale_opts --read-disambig-syms=$lang/phones/disambig.int $srcdir/tree $srcdir/final.mdl $lang/L_disambig.fst ark:- \
        ark,scp:$graph/HCLG.fsts.JOB.ark,$graph/fsts/HCLG.fsts.JOB.scp || exit 1
    cp $lang/words.txt $graph
    cp -r $lang/phones $graph

    for i in $(seq $nj); do cat $graph/fsts/HCLG.fsts.$i.scp; done > $graph/HCLG.fsts.scp

    n1=$(cat $work/text.int | wc -l)
    n2=$(cat $graph/HCLG.fsts.scp | wc -l)
    if [ $n1 -ne $n2 ]; then
        echo "$0: number of graphs not equal number of utts"
        exit 1
    fi
fi

if [ $stage -le 1 ]; then

	echo "$0: decoding $data using model from $srcdir, output in $work"
    lats=$work/lats
    mkdir -p $lats

    steps/cleanup/decode_segmentation.sh --acwt 1.0 --transform-dir $srcdir --beam $beam --nj $nj --cmd queue.pl --skip-scoring true $graph $data $lats $srcdir

    echo "$0: Doing oracle lookup"
    steps/cleanup/lattice_oracle_align.sh --cmd "$decode_cmd" $data $lang $lats $work
#	$decode_cmd JOB=1:$nj $work/log/decode.JOB.log \
#		gmm-latgen-faster --allow-partial=true --acoustic-scale=$acoustic_scale --beam=$beam \
#		--max-active=$max_active --lattice-beam=$lattice_beam \
#		--word-symbol-table=$lang/words.txt \
#		$srcdir/final.mdl $work/graph/HCLG.fst "$feats" ark:- \| \
#	lattice-oracle ark:- "ark:utils/sym2int.pl --map-oov \"$oov\" -f 2- $lang/words.txt $sdata/JOB/text|" \
#		ark,t:- ark,t:$work/edits.JOB.txt \| \
#	utils/int2sym.pl -f 2- $lang/words.txt '>' $work/aligned_ref.JOB.txt || exit 1;
fi

if [ $stage -le 2 ]; then

	for x in $(seq $nj); do cat $work/edits.$x.txt; done | awk '{if(NF==2){print;}}' > $work/edits.txt
	for x in $(seq $nj); do cat $work/aligned_ref.$x.txt; done | awk '{if(NF>=1){print;}}' > $work/aligned_ref.txt

	n1=$(wc -l < $work/edits.txt)
	n2=$(wc -l < $work/aligned_ref.txt)
	n3=$(wc -l < $data/text)

	if [ $n1 -ne $n2 ] || [ $n2 -ne $n3 ]; then
		echo "$0: mismatch in lengths of files:"
		wc $work/edits.txt $work/aligned_ref.txt $data/text
		exit 1;
	fi

	cat $data/text | awk '{print $1, (NF-1);}' > $work/length.txt

	paste $work/edits.txt <(awk '{print $2}' $work/length.txt) <(awk '{$1="";print;}' <$work/aligned_ref.txt)  <(awk '{$1="";print;}' <$data/text) > $work/all_info.txt

	sort -nr -k2 $work/all_info.txt > $work/all_info.sorted.txt

    awk '{er+=$2; ref+=$3} END {print er/ref}' $work/all_info.sorted.txt > $work/oracle_WER

fi

if [ $stage -le 3 ]; then

    awk '{er+=$2; ref+=$3; if (er/ref < 0.35) print $1}' $work/all_info.sorted.txt > $work/good_utts

fi
