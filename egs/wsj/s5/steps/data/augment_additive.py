#!/usr/bin/env python3
# Copyright 2017  David Snyder
#           2017  Ye Bai
# Apache 2.0
#
# This script generates augmented data.  It is based on
# steps/data/reverberate_data_dir.py but doesn't handle reverberation.
# It is designed to be somewhat simpler and more flexible for augmenting with
# additive noise.
from __future__ import print_function
import sys, random, argparse, os, imp
from glob import glob
sys.path.append("steps/data/")
from reverberate_data_dir import parse_file_to_dict
from reverberate_data_dir import write_dict_to_file
data_lib = imp.load_source('dml', 'steps/data/data_dir_manipulation_lib.py')

def GetArgs():
    parser = argparse.ArgumentParser(description="Augment the data directory with additive noises. "
        "Noises are separated into background and foreground noises which are added together or "
        "separately.  Background noises are added to the entire recording, and repeated as necessary "
        "to cover the full length.  Multiple overlapping background noises can be added, to simulate "
        "babble, for example.  Foreground noises are added sequentially, according to a specified "
        "interval.  See also steps/data/reverberate_data_dir.py "
        "Usage: augment_data_dir.py [options...] <in-data-dir> <out-data-dir> "
        "E.g., steps/data/augment_data_dir.py --utt-suffix aug --fg-snrs 20:10:5:0 --bg-snrs 20:15:10 "
        "--num-bg-noise 1:2:3 --fg-interval 3 --fg-noise-dir data/musan_noise --bg-noise-dir "
        "data/musan_music data/train data/train_aug", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--fg-snrs', type=str, dest = "fg_snr_str", default = '20:10:0',
                        help='When foreground noises are being added, the script will iterate through these SNRs.')
    parser.add_argument('--mg-snrs', type=str, dest="mg_snr_str", default='20:10:0',
                        help='When foreground noises are being added, the script will iterate through these SNRs.')
    parser.add_argument('--bg-snrs', type=str, dest = "bg_snr_str", default = '20:10:0',
                        help='When background noises are being added, the script will iterate through these SNRs.')
    parser.add_argument('--num-bg-noises', type=str, dest = "num_bg_noises", default = '1',
                        help='Number of overlapping background noises that we iterate over. For example, if the input is "1:2:3" then the output wavs will have either 1, 2, or 3 randomly chosen background noises overlapping the entire recording')
    parser.add_argument('--fg-interval', type=int, dest = "fg_interval", default = 10   ,
                        help='Number of seconds between the end of one foreground noise and the beginning of the next.')
    parser.add_argument('--utt-suffix', type=str, dest = "utt_suffix", default = "aug", help='Suffix added to utterance IDs.')
    parser.add_argument('--utt-prefix', type=str, dest = "utt_prefix", default = None, help='Suffix added to utterance IDs.')

    parser.add_argument("--bg-noise-dir", type=str, dest="bg_noise_dir",
                        help="Background noise data directory")
    parser.add_argument("--mg-noise-dir", type=str, dest="mg_noise_dir",
                        help="Background noise data directory")
    parser.add_argument("--fg-noise-dir", type=str, dest="fg_noise_dir",
                        help="Foreground noise data directory")
    parser.add_argument('--include-prob', type=float, dest = "include_prob", default = 1.0, help='Prob to augment file.')
    parser.add_argument('--rir-list', type=str, dest="rir_list", help='')
    parser.add_argument('--rir-list-spkr', type=str, dest="rir_list_spkr", help='')
    parser.add_argument("input_dir", help="Input data directory")
    parser.add_argument("output_dir", help="Output data directory")

    print(' '.join(sys.argv))
    print('WARNING: Current approach will corrupt speaker information!')
    args = parser.parse_args()
    args = CheckArgs(args)
    return args

def CheckArgs(args):
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    if not args.fg_interval >= 0:
        raise Exception("--fg-interval must be 0 or greater")
    if args.bg_noise_dir is None and args.fg_noise_dir is None:
        raise Exception("Either --fg-noise-dir or --bg-noise-dir must be specified")
    return args

def GetNoiseList(noise_wav_scp_filename):
    noise_wav_scp_file = open(noise_wav_scp_filename, 'r').readlines()
    noise_wavs = {}
    noise_utts = []
    for line in noise_wav_scp_file:
        toks=line.split(" ")
        wav = " ".join(toks[1:])
        noise_utts.append(toks[0])
        noise_wavs[toks[0]] = wav.rstrip()
    return noise_utts, noise_wavs


def AugmentWav(utt, wav, dur, fg_snr_opts, mg_snr_opts, bg_snr_opts, fg_noise_utts, \
               mg_noise_utts, bg_noise_utts, noise_wavs, noise2dur, interval, num_opts, lst_popsongs, lst_cafeenv, lst_backgenv,
               rir_list, rir_list_spkr, lst_simnoises):
    # This section is common to both foreground and background noises
    new_wav = ""
    dur_str = str(dur)
    noise_dur = 0

    point_noises = []
    point_snrs=[]
    point_start_times=[]

    noises = []
    snrs = []
    start_times = []


    no_augment_sources = ['AMI_', 'voices', 'SBC0', 'scotparl']

    is_less_augment_src = any([src in utt for src in no_augment_sources])
    doing_reverb = (random.random() < 0.5 and not is_less_augment_src)
    doing_speaker_reverb = random.random() < 0.25 and not is_less_augment_src
    adding_speech = random.random() < 0.5
    adding_fg_noise = random.random() < 0.5
    adding_mg_noise = random.random() < 0.5
    adding_cafe = random.random() < 0.5

    if not is_less_augment_src and adding_cafe:
        noisepath = random.choice(lst_cafeenv)
        noise = "wav-reverberate --duration={} \"{}\" - |".format(dur_str, noisepath)
        snr = random.choice(['5', '9', '15'])
        if doing_speaker_reverb:
            snr = str(int(snr) + 3)
        snrs.append(snr)
        start_times.append(0)
        noises.append(noise)
        snr = str(30 - int(snr))
        if not adding_speech:
            snr = str(int(snr) - 6)
        if doing_speaker_reverb:
            snr = str(int(snr) + 3)

        snrs.append(snr)
        start_times.append(0)
        noisepath = random.choice(lst_backgenv)
        noise = "wav-reverberate --duration={} \"{}\" - |".format(dur_str, noisepath)
        noises.append(noise)

    # This is speech
    if len(bg_noise_utts) > 0 and adding_speech:
        num = random.choice(num_opts)
        snr_base = random.choice(bg_snr_opts)
        for i in range(0, num):
            noise_utt = random.choice(bg_noise_utts)
            noise = "wav-reverberate --duration=" \
                    + dur_str + " \"" + noise_wavs[noise_utt] + "\" - |"
            snr = str(int(snr_base) + random.randint(-1, 1))
            if is_less_augment_src:
                snr = str(int(snr) + 8)
            elif not adding_cafe:
                snr = str(int(snr) - 4)
            point_snrs.append(snr)
            point_start_times.append(0)
            point_noises.append(noise)

    if random.random() < 0.25 and not is_less_augment_src:
        pop_path = random.choice(lst_popsongs)
        snr = random.choice(['23', '25'])
        point_snrs.append(snr)
        point_start_times.append(0)
        noise = "wav-reverberate --duration={} \"{}\" - |".format(dur_str, pop_path)
        point_noises.append(noise)

    tot_noise_dur = 0
    if len(mg_noise_utts) > 0 and adding_mg_noise:
        while tot_noise_dur < dur:
            noise_utt = random.choice(mg_noise_utts)
            noise = noise_wavs[noise_utt]
            snr = random.choice(mg_snr_opts)
            if is_less_augment_src:
                snr = str(int(snr) + 5)
            point_snrs.append(snr)
            noise_dur = round(noise2dur[noise_utt], 1)
            point_start_times.append(tot_noise_dur)
            offset = random.choice(list(range(-2, interval)))
            tot_noise_dur += noise_dur + offset
            tot_noise_dur = max(tot_noise_dur, 0)
            point_noises.append(noise)

    tot_noise_dur = 0
    # Now handle the foreground noises
    if len(fg_noise_utts) > 0 and adding_fg_noise:
        while tot_noise_dur < dur:
            noise_utt = random.choice(fg_noise_utts)
            noise = noise_wavs[noise_utt]
            snr = random.choice(fg_snr_opts)
            if is_less_augment_src:
                snr = str(int(snr) + 10)
            elif adding_cafe:
                snr = str(int(snr) + 5)
            elif doing_speaker_reverb:
                snr = str(int(snr) + 3)
            point_snrs.append(snr)
            noise_dur = round(noise2dur[noise_utt], 1)
            point_start_times.append(tot_noise_dur)
            offset = random.choice(list(range(-2, interval)))
            tot_noise_dur += noise_dur + offset
            tot_noise_dur = max(tot_noise_dur, 0)
            point_noises.append(noise)

    rir_str = ""
    rir_str_spkr = ""
    if doing_reverb:
        rir_path = random.choice(rir_list)
        rir_str = '--impulse-response="sox {} -c 1 -b 16 -r 16000 -t wav - |"'.format(rir_path)
        if doing_speaker_reverb:
            rir_path = random.choice(rir_list_spkr)
            rir_str_spkr = '--impulse-response-speaker="sox {} -c 1 -b 16 -r 16000 -t wav - |"'.format(rir_path)

    if random.random() < 0.5 or not (adding_speech or adding_mg_noise or adding_fg_noise):
        start_times.append(0)
        simnoise_path = random.choice(lst_simnoises)
        snr = random.choice(['15', '20', '25'])
        if adding_cafe or doing_speaker_reverb:
            snr = str(int(snr) + 5)
        snrs.append(snr)
        noise = "wav-reverberate --duration={} \"{}\" - |".format(dur_str, simnoise_path)
        noises.append(noise)

    start_times_str = ""
    snrs_str = ""
    noises_str = ""
    if len(noises):
        start_times_str = "--start-times='" + ",".join([str(i)[:str(i).rfind('.')+2] for i in start_times]) + "'"
        snrs_str = "--snrs='" + ",".join([str(i) for i in snrs]) + "'"
        noises_str = "--additive-signals='" + ",".join(noises).strip() + "'"

    point_start_times_str = "--start-times-point='" + ",".join([str(i)[:str(i).rfind('.') + 2] for i in point_start_times]) + "'"
    point_snrs_str = "--snrs-point='" + ",".join([str(i) for i in point_snrs]) + "'"
    point_noises_str = "--point-additive-signals='" + ",".join(point_noises).strip() + "'"

    vol_str = ""
    if random.random() < 0.5:
        vol_adj = random.random() * 0.5 + 0.33
        vol_str = "--volume={}".format(vol_adj)

    # If the wav is just a file
    if wav.strip()[-1] != "|":
        new_wav = "wav-reverberate --shift-output=true " + rir_str + " " + noises_str + " " \
                  + start_times_str + " " + snrs_str + " " \
                  + rir_str_spkr + " " + point_noises_str + " " + point_start_times_str + " " + point_snrs_str + " " + vol_str+" " +wav + " - |"

    # Else if the wav is in a pipe
    else:
        raise RuntimeError
        new_wav = "wav-reverberate --shift-output=true " + rir_str + " " + noises_str + " " \
                  + start_times_str + " " + snrs_str + " " \
                  + rir_str_spkr + " " + point_noises_str + " " + point_start_times_str + " " + point_snrs_str + " "+wav + " - |"
    return new_wav


def CopyFileIfExists(utt_suffix, filename, input_dir, output_dir, utt_prefix=None):
    if os.path.isfile(input_dir + "/" + filename):
        dict = parse_file_to_dict(input_dir + "/" + filename,
            value_processor = lambda x: " ".join(x))
        if utt_prefix is None:
            if len(utt_suffix) > 0:
                new_dict = {}
                for key in dict.keys():
                    new_dict[key + "-" + utt_suffix] = dict[key]
                dict = new_dict
        else:

            if len(utt_prefix) > 0:
                new_dict = {}
                for key in dict.keys():
                    new_dict[utt_prefix + '-' + key] = dict[key]
                dict = new_dict
        write_dict_to_file(dict, output_dir + "/" + filename)

def main():
    args = GetArgs()
    fg_snrs = [int(i) for i in args.fg_snr_str.split(":")]
    bg_snrs = [int(i) for i in args.bg_snr_str.split(":")]
    mg_snrs = [int(i) for i in args.mg_snr_str.split(":")]
    input_dir = args.input_dir
    output_dir = args.output_dir
    num_bg_noises = [int(i) for i in args.num_bg_noises.split(":")]
    reco2dur = parse_file_to_dict(input_dir + "/reco2dur",
        value_processor = lambda x: float(x[0]))
    wav_scp_file = open(input_dir + "/wav.scp", 'r').readlines()

    noise_wavs = {}
    noise_reco2dur = {}
    bg_noise_utts = []
    fg_noise_utts = []

    # Load background noises
    if args.bg_noise_dir:
        bg_noise_wav_filename = args.bg_noise_dir + "/wav.scp"
        bg_noise_utts, bg_noise_wavs = GetNoiseList(bg_noise_wav_filename)
        bg_noise_reco2dur = parse_file_to_dict(args.bg_noise_dir + "/reco2dur",
            value_processor = lambda x: float(x[0]))
        noise_wavs.update(bg_noise_wavs)
        noise_reco2dur.update(bg_noise_reco2dur)

    if args.mg_noise_dir:
        mg_noise_wav_filename = args.mg_noise_dir + '/wav.scp'
        mg_noise_utts, mg_noise_wavs = GetNoiseList(mg_noise_wav_filename)
        bg_noise_reco2dur = parse_file_to_dict(args.mg_noise_dir + "/reco2dur",
                                            value_processor=lambda x: float(x[0]))
        noise_wavs.update(mg_noise_wavs)
        noise_reco2dur.update(bg_noise_reco2dur)

    # Load background noises
    if args.fg_noise_dir:
        fg_noise_wav_filename = args.fg_noise_dir + "/wav.scp"
        fg_noise_utts, fg_noise_wavs = GetNoiseList(fg_noise_wav_filename)
        fg_noise_reco2dur = parse_file_to_dict(args.fg_noise_dir + "/reco2dur",
                                            value_processor = lambda x: float(x[0]))
        noise_wavs.update(fg_noise_wavs)
        noise_reco2dur.update(fg_noise_reco2dur)

    random.seed(123)
    new_utt2wav = {}
    new_utt2spk = {}

    lst_popsongs = glob('/work/noise/popsongs/*wav')
    with open('/work/noise/popsongs/reco2dur') as fh:
        for line in fh:
            path, dur = line.split()
            fname = os.path.basename(path)
            noise_reco2dur[fname] = float(dur)
    lst_cafeenv = glob('/work/noise/custom_variablestate/*wav')
    lst_backgenv = open('/work/noise/custom_steadystate/noiselist').read().splitlines()
    lst_simnoises = glob('/work/noise/sim-noises/*wav')
    lst_rir = []
    with open(args.rir_list) as fh:
        for line in fh:
            *start, fpath = line.split()
            lst_rir.append(fpath)

    lst_rir_spkr = []
    with open(args.rir_list_spkr) as fh:
        for line in fh:
            *start, fpath = line.split()
            lst_rir_spkr.append(fpath)

    utt2spk = {}
    with open(os.path.join(input_dir, 'utt2spk')) as fh:
        for line in fh:
            line = line.split()
            utt2spk[line[0]] = line[1]

    numutts = len(utt2spk)
    randomnums = [random.random() for _ in range(numutts)]
    # Augment each line in the wav file
    for line in wav_scp_file:
        toks = line.rstrip().split(" ")
        utt = toks[0]
        if 'voices' in utt:
            continue
        wav = " ".join(toks[1:])
        dur = reco2dur[utt]
        new_wav = AugmentWav(utt, wav, dur, fg_snrs, mg_snrs, bg_snrs, fg_noise_utts,
            mg_noise_utts, bg_noise_utts, noise_wavs, noise_reco2dur, args.fg_interval,
            num_bg_noises, lst_popsongs, lst_cafeenv, lst_backgenv, lst_rir, lst_rir_spkr, lst_simnoises)
        if args.utt_prefix is None:
            new_utt = utt + "-" + args.utt_suffix
        else:
            new_utt = args.utt_prefix + '-' + utt
        new_utt2wav[new_utt] = new_wav

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    write_dict_to_file(new_utt2wav, output_dir + "/wav.scp")

    kept_utts = set()
    seg_lines = []
    with open(os.path.join(input_dir, 'segments')) as fh:
        for i, line in enumerate(fh):
            uttid, wavid, s, e = line.split()
            if randomnums[i] > args.include_prob:
                continue

            newwavid = None
            if args.utt_prefix is not None:
                newwavid = args.utt_prefix + '-' + wavid
            else:
                newwavid = wavid + '-' + args.utt_suffix
            if newwavid in new_utt2wav:
                if args.utt_prefix is not None:
                    newuttid = args.utt_prefix + '-' + uttid
                    new_utt2spk[newuttid] = args.utt_prefix + '-' + utt2spk[uttid]
                else:
                    newuttid = uttid + '-' + args.utt_suffix
                    new_utt2spk[newuttid] = utt2spk[uttid]
                kept_utts.add(newuttid)
                l = '{} {} {} {}'.format(newuttid, newwavid, s, e)
                seg_lines.append(l)

    with open(os.path.join(output_dir, 'segments'), 'w') as fh:
        for line in seg_lines:
            fh.write('{}\n'.format(line))

    with open(os.path.join(output_dir, 'utt2spk'), 'w') as fh:
        for utt, spk in new_utt2spk.items():
            if utt in kept_utts:
                fh.write('{} {}\n'.format(utt, spk))

    CopyFileIfExists(args.utt_suffix, "reco2dur", input_dir, output_dir, args.utt_prefix)
    CopyFileIfExists(args.utt_suffix, "utt2dur", input_dir, output_dir, args.utt_prefix)
    CopyFileIfExists(args.utt_suffix, "text", input_dir, output_dir, args.utt_prefix)
    CopyFileIfExists("", "spk2gender", input_dir, output_dir)
    data_lib.RunKaldiCommand("utils/fix_data_dir.sh {output_dir}".format(output_dir = output_dir))

if __name__ == "__main__":
    main()
